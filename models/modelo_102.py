import numpy as np

dimension = 2
time_max= 10  #tiempo maximo de conputo permitido
error=0.000001 #CR3 se usa e para comparar si el CR3 es true o false


#Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10,10])
   
#Creo el vector limit_low_V, limite inferior   
limit_low_V = np.array([-10,-10])

solution_position_V=np.array([0]*dimension)
solution_global=-186

def function_serial(x, particles_number, limit_up_V, limit_low_V):  #Shubert Function
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        a = 0
        b = 0
        for j in range(5):
            a = a + (j+1) * np.cos((j + 2) * x[i][0] + (j+1))
            b = b + (j+1) * np.cos((j + 2) * x[i][1] + (j+1))  #donde dice j, puse j+1 pq el contador empiezza en cero. donde dice (i+2) iba (i+1),pero por la misma logica le puse i+2
        f[i] = a*b
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        TAV[i]=glimites[i]
    return f,TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float a[particles_number];
            float b[particles_number];

            a[i]= 0;
            b[i] = 0;
            for (int jj=0; jj<5; jj++){
                a[i]= a[i]+ (jj+1) * cos((jj + 2) * x[i*dimension+0] + (jj+1));
                b[i] = b[i] + (jj+1) * cos((jj + 2) * x[i*dimension+1] + (jj+1));  //donde dice i, puse i+1 pq el contador empieza en cero. donde dice (i+2) iba (i+1),pero por la misma logica le puse i+2
            }

            f[i]= a[i]*b[i];

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            TAV[i]=glimites[i];
"""

