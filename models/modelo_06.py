import numpy as np

dimension = 2
time_max = 10  # tiempo maximo de conputo permitido
error = 0.000001  # CR3 se usa error para comparar si el CR3 es true o false

# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([100, 100])
# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([13, 0])

solution_position_V = np.array([14.09500000000000064, 0.8429607892154795668])
solution_global = -6961.81387558015


def function_serial(x, particles_number, limit_up_V, limit_low_V):  # funcion g06 --> comprobe q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i] = (x[i][0] - 10) ** 3 + (x[i][1] - 20) ** 3
        g1 = -(x[i][0] - 5) ** 2 - (x[i][1] - 5) ** 2 + 100
        g2 = (x[i][0] - 6) ** 2 + (x[i][1] - 5) ** 2 - 82.81
        gg1 = max(0, g1)
        gg2 = max(0, g2)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g = gg1 + gg2
        TAV[i] = g + glimites[i]
    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float g1[particles_number];
            float g2[particles_number];
            float gg1[particles_number];
            float gg2[particles_number];
            float gg[particles_number];

            f_s[i]=pow((x_s[i*dimension+0]-10),3) + pow((x_s[i*dimension+1]-20),3);

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x_s[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x_s[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }


            g1[i]=-pow((x_s[i*dimension+0] - 5),2) - pow((x_s[i*dimension+1] - 5),2) + 100;
            g2[i]=pow((x_s[i*dimension+0] - 6),2) + pow((x_s[i*dimension+1] - 5),2) - 82.81;

            gg1[i]=0;
            gg2[i]=0;

            if (g1[i]>0) {
            gg1[i]=g1[i];
            }
            if (g2[i]>0) {
            gg2[i]=g2[i];
            }

            gg[i]=gg1[i]+gg2[i];

            TAV_s[i]=glimites[i]+gg[i];
"""
