import numpy as np

dimension = 10
time_max= 10  #tiempo maximo de conputo permitido
error=0.01 #CR3 se usa error para comparar si el CR3 es true o false

#Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10,10,10,10,10,10,10,10,10,10])

#Creo el vector limit_low_V, limite inferior   
limit_low_V = np.array([-10,-10,-10,-10,-10,-10,-10,-10,-10,-10])

solution_position_V=np.array([2.17199634142692,2.3636830416034,8.77392573913157,5.09598443745173,0.990654756560493, 1.43057392853463,1.32164415364306,9.82872576524495,8.2800915887356,8.3759266477347])
solution_global=24.30620906818



def function_serial(x,particles_number,limit_up_V,limit_low_V):  #function_id_serial g07 --> comprobe q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number,dimension))
    glimiteinferior = np.zeros((particles_number,dimension))
    gglimitesuperior = np.zeros((particles_number,dimension))
    gglimiteinferior = np.zeros((particles_number,dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i]=(x[i][0])**2+(x[i][1])**2+x[i][0]*x[i][1]-14*x[i][0]-16*x[i][1]+(x[i][2]-10)**2 + 4*(x[i][3]-5)**2+(x[i][4]-3)**2+2*(x[i][5]-1)**2+5*(x[i][6])**2+7*(x[i][7]-11)**2 + 2*(x[i][8]-10)**2+(x[i][9]-7)**2+45
        g1= -105+4*x[i][0]+5*x[i][1]-3*x[i][6]+9*x[i][7]
        g2= 10*x[i][0]-8*x[i][1]-17*x[i][6]+2*x[i][7]
        g3= -8*x[i][0]+2*x[i][1]+5*x[i][8]-2*x[i][9]-12
        g4= 3*(x[i][0]-2)**2 + 4*(x[i][1]-3)**2 + 2*(x[i][2])**2-7*x[i][3]-120
        g5= 5*(x[i][0])**2+8*x[i][1]+(x[i][2]-6)**2-2*x[i][3]-40
        g6= (x[i][0])**2+2*(x[i][1]-2)**2-2*x[i][0]*x[i][1]+14*x[i][4]-6*x[i][5]
        g7= 0.5*(x[i][0]-8)**2 + 2*(x[i][1]-4)**2 + 3*(x[i][4])**2-x[i][5]-30
        g8= -3*x[i][0]+6*x[i][1]+12*(x[i][8]-8)**2-7*x[i][9]
        gg1=max(0,g1)
        gg2=max(0,g2)
        gg3=max(0,g3)
        gg4=max(0,g4)
        gg5=max(0,g5)
        gg6=max(0,g6)
        gg7=max(0,g7)
        gg8=max(0,g8)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g=gg1+gg2+gg3+gg4+gg5+gg6+gg7+gg8
        TAV[i]=g+glimites[i]
    return f,TAV

function_parallel = """
        float glimitesuperior[particles_number*dimension+dimension];
        float glimiteinferior[particles_number*dimension+dimension];
        float glimites[particles_number];
        float g1[particles_number];
        float g2[particles_number];
        float g3[particles_number];
        float g4[particles_number];
        float g5[particles_number];
        float g6[particles_number];
        float g7[particles_number];
        float g8[particles_number];
        float gg1[particles_number];
        float gg2[particles_number];
        float gg3[particles_number];
        float gg4[particles_number];
        float gg5[particles_number];
        float gg6[particles_number];
        float gg7[particles_number];
        float gg8[particles_number];
        float gg[particles_number];

        f[i]= pow((x[i*dimension+0]),2)+pow(x[i*dimension+1],2)+x[i*dimension+0]*x[i*dimension+1]-14*x[i*dimension+0]-16*x[i*dimension+1]+pow((x[i*dimension+2]-10),2) + 4*pow((x[i*dimension+3]-5),2)+pow((x[i*dimension+4]-3),2)+2*pow((x[i*dimension+5]-1),2)+5*pow((x[i*dimension+6]),2)+7*pow((x[i*dimension+7]-11),2) + 2*pow((x[i*dimension+8]-10),2)+pow((x[i*dimension+9]-7),2)+45;

        glimites[i]=0;

        for (int jjj=0; jjj<dimension; jjj++){
            glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
            glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

            if (glimitesuperior[i*dimension+jjj]<0) {

            glimitesuperior[i*dimension+jjj]=0;

            }

            if (glimiteinferior[i*dimension+jjj]<0) {

            glimiteinferior[i*dimension+jjj]=0;

            }

            glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
        }

        g1[i] = -105+4*x[i*dimension+0]+5*x[i*dimension+1]-3*x[i*dimension+6]+9*x[i*dimension+7];
        g2[i] = 10*x[i*dimension+0]-8*x[i*dimension+1]-17*x[i*dimension+6]+2*x[i*dimension+7];
        g3[i] = -8*x[i*dimension+0]+2*x[i*dimension+1]+5*x[i*dimension+8]-2*x[i*dimension+9]-12;
        g4[i] = 3*pow((x[i*dimension+0]-2),2) + 4*pow((x[i*dimension+1]-3),2) + 2*pow((x[i*dimension+2]),2)-7*x[i*dimension+3]-120;
        g5[i] = 5*pow((x[i*dimension+0]),2)+8*x[i*dimension+1]+pow((x[i*dimension+2]-6),2)-2*x[i*dimension+3]-40;
        g6[i] = pow(x[i*dimension+0],2)+2*pow((x[i*dimension+1]-2),2)-2*x[i*dimension+0]*x[i*dimension+1]+14*x[i*dimension+4]-6*x[i*dimension+5];
        g7[i] = 0.5*pow((x[i*dimension+0]-8),2) + 2*pow((x[i*dimension+1]-4),2) + 3*pow(x[i*dimension+4],2)-x[i*dimension+5]-30;
        g8[i] = -3*x[i*dimension+0]+6*x[i*dimension+1]+12*pow((x[i*dimension+8]-8),2)-7*x[i*dimension+9];

        gg1[i]=0;
        gg2[i] = 0;
        gg3[i] = 0;
        gg4[i] = 0;
        gg5[i] = 0;
        gg6[i] = 0;
        gg7[i] = 0;
        gg8[i] = 0;

        if (g1[i]>0) {
            gg1[i]=g1[i];
        }
        if (g2[i]>0) {
            gg2[i] = g2[i];
        }
        if (g3[i]>0) {
            gg3[i] = g3[i];
        }
        if (g4[i]>0) {
            gg4[i] = g4[i];
        }
        if (g5[i]>0) {
            gg5[i] = g5[i];
        }
        if (g6[i]>0) {
            gg6[i] = g6[i];
        }
        if (g7[i]>0) {
            gg7[i] = g7[i];
        }
        if (g8[i]>0) {
            gg8[i] = g8[i];
        }

        gg[i]=gg1[i]+gg2[i]+gg3[i]+gg4[i]+gg5[i]+gg6[i]+gg7[i]+gg8[i];
        TAV[i]=gg[i];

        TAV[i]=glimites[i]+gg[i];
"""

