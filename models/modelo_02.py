import numpy as np

dimension = 20
time_max = 10  # tiempo maximo de conputo permitido
error = 0.000000001  # CR3 se usa error para comparar si el CR3 es true o false

# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10] * dimension)

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([0] * dimension)

solution_position_V = np.array(
    [3.16246061572185, 3.12833142812967, 3.09479212988791, 3.06145059523469, 3.02792915885555, 2.99382606701730,
     2.95866871765285, 2.9218422731245, 0.49482511456933, 0.48835711005490, 0.48231642711865, 0.47664475092742,
     0.47129550835493, 0.46623099264167, 0.46142004984199, 0.45683664767217, 0.45245876903267, 0.44826762241853,
     0.44424700958760, 0.44038285956317])
solution_global = -0.80361910412559


# funcion con restricciones en el TAV
def function_serial(x, particles_number, limit_up_V, limit_low_V):
    f = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)

    for i in range(particles_number):
        s1 = 0
        s2 = 0
        s3 = 0
        p1 = 1
        p2 = 1
        for j in range(dimension):
            s1 = s1 + np.power(np.cos(x[i][j]), 4)
            s2 = s2 + (j + 1) * np.power((x[i][j]), 2)
            s3 = s3 + x[i][j]
            p1 = p1 * np.power(np.cos(x[i][j]), 2)
            p2 = p2 * x[i][j]

        f[i] = -np.abs((s1 - 2 * p1) / np.sqrt(s2))

        g1 = 0.75 - p2
        g2 = s3 - 7.8*dimension

        gg1 = max(0, g1)
        gg2 = max(0, g2)

        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        gg = gg1 + gg2

        TAV[i] = gg + glimites[i]
    return f, TAV


function_parallel = """
                    float glimitesuperior[particles_number*dimension+dimension];
                    float glimiteinferior[particles_number*dimension+dimension];
                    float glimites[particles_number];
                    float ss1[particles_number];
                    float ss2[particles_number];
                    float ss3[particles_number];
                    float pp1[particles_number];
                    float pp2[particles_number];
                    float g1[particles_number];
                    float g2[particles_number];
                    float gg1[particles_number];
                    float gg2[particles_number];
                    float gg[particles_number];


                    ss1[i]=0;
                    ss2[i]=0;
                    ss3[i]=0;
                    pp1[i]=1;
                    pp2[i]=1;

                    for (int jjj=0; jjj<dimension; jjj++){
                        ss1[i]=ss1[i]+pow(cos(x[i*dimension+jjj]),4);
                        ss2[i]=ss2[i]+ (j + 1)*pow(x[i*dimension+jjj],2);
                        ss3[i]=ss3[i]+x[i*dimension+jjj];
                        pp1[i]= pp1[i] * pow(cos(x[i*dimension+jjj]), 2);
                        pp2[i]= pp2[i] * x[i*dimension+jjj];

                    }
                    f[i]= -abs((ss1[i] - 2 * pp1[i]) / sqrt(float(ss2[i])));

                    glimites[i]=0;

                    for (int jjj=0; jjj<dimension; jjj++){
                        glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                        glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                        if (glimitesuperior[i*dimension+jjj]<0) {

                        glimitesuperior[i*dimension+jjj]=0;

                        }

                        if (glimiteinferior[i*dimension+jjj]<0) {

                        glimiteinferior[i*dimension+jjj]=0;

                        }

                        glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
                    }


                    g1[i]= 0.75 - pp2[i];
                    g2[i]= ss3[i] - 7.8 * dimension;

                    gg1[i]=0;
                    gg2[i]=0;

                    if (g1[i]>0) {
                    gg1[i]=g1[i];
                    }
                    if (g2[i]>0) {
                    gg2[i]=g2[i];
                    }

                    gg[i]=gg1[i]+gg2[i];

                    TAV[i]= gg[i] + glimites[i];
"""
