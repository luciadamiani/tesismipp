import numpy as np

dimension = 10
time_max = 10  # tiempo maximo de conputo permitido
error = 0.00001  # CR3 se usa e para comparar si el CR3 es true o false

# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10, 10, 10, 10, 10, 10, 10, 10, 10, 10])

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001])

solution_position_V=np.array([0.0406684113216282,0.147721240492452,0.783205732104114,0.00141433931889084,0.485293636780388,0.000693183051556082,0.0274052040687766,0.0179509660214818,0.0373268186859717,0.0968844604336845])
solution_global=-47.7648884594915



c = np.array([-6.089, -17.164, -34.054, -5.914, -24.721, -14.986, -24.1, -10.708, -26.662, -22.179])


def function_serial(x,particles_number,limit_up_V,limit_low_V):
    glimitesuperior = np.zeros((particles_number,dimension))
    glimiteinferior = np.zeros((particles_number,dimension))
    gglimitesuperior = np.zeros((particles_number,dimension))
    gglimiteinferior = np.zeros((particles_number,dimension))
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        sum = 0
        k = 0
        for j in range(dimension):
            sum = x[i][j] + sum
        for j in range(dimension):
            if (x[i][j] / sum)>0:
                k = x[i][j] * (c[j] + np.log(x[i][j] / sum)) + k  # np.log=ln
        f[i] = k
        h1 = x[i][0] + 2 * x[i][1] + 2 * x[i][2] + x[i][5] + x[i][9] - 2
        h2 = x[i][3] + 2 * x[i][4] + x[i][5] + x[i][6] - 1
        h3 = x[i][2] + x[i][6] + x[i][7] + 2 * x[i][8] + x[i][9] - 1
        h = abs(h1) + abs(h2) + abs(h3)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        TAV[i] = h + glimites[i]
    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float sum[particles_number];
            float kkk[particles_number];
            float h1[particles_number];
            float h2[particles_number];
            float h3[particles_number];
            float h[particles_number];



            float c[]={-6.089,-17.164,-34.054,-5.914,-24.721,-14.986,-24.1,-10.708,-26.662,-22.179};
            sum[i]=0;
            kkk[i]=0;
            for (int jjj=0;jjj<dimension; jjj++){
                sum[i]=x[i*dimension+jjj]+sum[i];
            }
            for (int jjj=0;jjj<dimension; jjj++){
                if ((x[i*dimension+jjj] / sum[i])>0){
                    kkk[i]=x[i*dimension+jjj]*(c[jjj]+log(float(x[i*dimension+jjj])/sum[i]))+kkk[i];
                }
            }
            f[i]= kkk[i];

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            h1[i]= x[i*dimension+0]+2*x[i*dimension+1]+2*x[i*dimension+2]+x[i*dimension+5]+x[i*dimension+9]-2;
            h2[i]= x[i*dimension+3]+2*x[i*dimension+4]+x[i*dimension+5]+x[i*dimension+6]-1;
            h3[i]= x[i*dimension+2]+x[i*dimension+6]+x[i*dimension+7]+2*x[i*dimension+8]+x[i*dimension+9]-1;
            h[i]= abs(h1[i])+abs(h2[i])+abs(h3[i]);

            TAV[i]=glimites[i]+h[i];
"""

