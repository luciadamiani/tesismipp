import numpy as np

dimension = 2
time_max = 10  # tiempo maximo de conputo permitido
error = 0.0001  # CR3 se usa e para comparar si el CR3 es true o false

# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10, 10])

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([0.0001, 0.0001])

solution_position_V = np.array([1.22797135260752599, 4.24537336612274885])
solution_global = -0.0958250414180359


def function_serial(x, particles_number, limit_up_V, limit_low_V):  # function_id_serial g08 --> comprobe q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i] = -(np.sin(2 * np.pi * x[i][1]) * (np.sin(2 * np.pi * x[i][0])) ** 3) / (
        (x[i][0]) ** 3 * (x[i][0] + x[i][1]))
        g1 = (x[i][0]) ** 2 - x[i][1] + 1
        g2 = 1 - x[i][0] + (x[i][1] - 4) ** 2
        gg1 = max(0, g1)
        gg2 = max(0, g2)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g = gg1 + gg2
        TAV[i] = g + glimites[i]

    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float g1[particles_number];
            float g2[particles_number];
            float gg1[particles_number];
            float gg2[particles_number];
            float gg[particles_number];

            f[i]=-(sin(2*M_PI*x[i*dimension+1])*pow((sin(2*M_PI*x[i*dimension+0])),3))/(pow(x[i*dimension+0],3)*(x[i*dimension+0]+x[i*dimension+1]));

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }
                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }


            g1[i]= pow(x[i*dimension+0],2)-x[i*dimension+1]+1;

            g2[i]= 1-x[i*dimension+0]+pow((x[i*dimension+1]-4),2);

            gg1[i]=0;
            gg2[i]=0;

            if (g1[i]>0) {
                gg1[i]=g1[i];
            }
            if (g2[i]>0) {
                gg2[i]=g2[i];
            }

             gg[i]=gg1[i]+gg2[i];

             TAV[i]=gg[i]+glimites[i] ;


 """

