import numpy as np

dimension = 10
time_max= 10  #tiempo maximo de conputo permitido
error=0.000001 #CR3 se usa e para comparar si el CR3 es true o false

#Creo el vector limit_up_V, limite superior
limit_up_V = np.array([5.12]*dimension)

#Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([-5.12]*dimension)

solution_position_V=np.array([0]*dimension)
solution_global=0

def function_serial(x, particles_number, limit_up_V, limit_low_V):  #RASTRIGRIN
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        ss = 0
        for j in range(dimension):
            ss = ss+pow(x[i][j],2)-10*np.cos(2*np.pi*x[i][j])
        f[i] = 10*dimension+ss
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i]+ gglimitesuperior[i][j] + gglimiteinferior[i][j]
        TAV[i]=glimites[i]
    return f,TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float ss[particles_number];

            ss[i] = 0;

            for (int jjj=0; jjj<dimension; jjj++){
                ss[i]=ss[i]+pow(x[i*dimension+jjj],2)-10*cos(float(2*float(M_PI)*x[i*dimension+jjj]));
            }

            f[i]=10*dimension+ss[i];

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            TAV[i]=glimites[i];
"""