import numpy as np

dimension = 13

time_max = 10  # tiempo maximo de conputo permitido
error = 0.000000001  # CR3 se usa error para comparar si el CR3 es true o false

# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 100, 100, 100, 1])

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

solution_position_V = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 1])
solution_global = -15


# function_id_serial con restricciones en el TAV
def function_serial(x, particles_number, limit_up_V, limit_low_V):
    f = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)

    for i in range(particles_number):
        s1 = 0
        s2 = 0
        s3 = 0
        for j in range(4):
            s1 = s1 + x[i][j]
            s2 = s2 + (x[i][j]) ** 2

        for j in range(4, 13):
            s3 = s3 + x[i][j]
        f[i] = 5 * s1 - 5 * s2 - s3
        g1 = 2 * x[i][0] + 2 * x[i][1] + x[i][9] + x[i][10] - 10
        g2 = 2 * x[i][0] + 2 * x[i][2] + x[i][9] + x[i][11] - 10
        g3 = 2 * x[i][1] + 2 * x[i][2] + x[i][10] + x[i][11] - 10
        g4 = -8 * x[i][0] + x[i][9]
        g5 = -8 * x[i][1] + x[i][10]
        g6 = -8 * x[i][2] + x[i][11]
        g7 = -2 * x[i][3] - x[i][4] + x[i][9]
        g8 = -2 * x[i][5] - x[i][6] + x[i][10]
        g9 = -2 * x[i][7] - x[i][8] + x[i][11]
        gg1 = max(0, g1)
        gg2 = max(0, g2)
        gg3 = max(0, g3)
        gg4 = max(0, g4)
        gg5 = max(0, g5)
        gg6 = max(0, g6)
        gg7 = max(0, g7)
        gg8 = max(0, g8)
        gg9 = max(0, g9)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        gg = gg1 + gg2 + gg3 + gg4 + gg5 + gg6 + gg7 + gg8 + gg9
        TAV[i] = gg + glimites[i]
    return f, TAV


function_parallel = """
                    float glimitesuperior[particles_number*dimension+dimension];
                    float glimiteinferior[particles_number*dimension+dimension];
                    float glimites[particles_number];
                    float ss1[particles_number];
                    float ss2[particles_number];
                    float ss3[particles_number];
                    float g1[particles_number];
                    float g2[particles_number];
                    float g3[particles_number];
                    float g4[particles_number];
                    float g5[particles_number];
                    float g6[particles_number];
                    float g7[particles_number];
                    float g8[particles_number];
                    float g9[particles_number];
                    float gg1[particles_number];
                    float gg2[particles_number];
                    float gg3[particles_number];
                    float gg4[particles_number];
                    float gg5[particles_number];
                    float gg6[particles_number];
                    float gg7[particles_number];
                    float gg8[particles_number];
                    float gg9[particles_number];
                    float gg[particles_number];

                    ss1[i]=0;
                    ss2[i]=0;
                    ss3[i]=0;
                    for (int jjj=0; jjj<4; jjj++){
                        ss1[i]=ss1[i]+x[i*dimension+jjj];
                        ss2[i]=ss2[i]+pow(x[i*dimension+jjj],2);
                    }
                    for (int jjj=4; jjj<13; jjj++){
                        ss3[i]=ss3[i]+x[i*dimension+jjj];
                    }
                    f[i]=5*ss1[i]-5*ss2[i]-ss3[i];

                    glimites[i]=0;

                    for (int jjj=0; jjj<dimension; jjj++){
                        glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                        glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                        if (glimitesuperior[i*dimension+jjj]<0) {

                        glimitesuperior[i*dimension+jjj]=0;

                        }

                        if (glimiteinferior[i*dimension+jjj]<0) {

                        glimiteinferior[i*dimension+jjj]=0;

                        }

                        glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
                    }

                    g1[i]=2*x[i*dimension+0]+2*x[i*dimension+1]+x[i*dimension+9]+x[i*dimension+10]-10;
                    g2[i]=2*x[i*dimension+0]+2*x[i*dimension+2]+x[i*dimension+9]+x[i*dimension+11]-10;
                    g3[i]=2*x[i*dimension+1]+2*x[i*dimension+2]+x[i*dimension+10]+x[i*dimension+11]-10;
                    g4[i]=-8*x[i*dimension+0]+x[i*dimension+9];
                    g5[i]=-8*x[i*dimension+1]+x[i*dimension+10];
                    g6[i]=-8*x[i*dimension+2]+x[i*dimension+11];
                    g7[i]=-2*x[i*dimension+3]-x[i*dimension+4]+x[i*dimension+9];
                    g8[i]=-2*x[i*dimension+5]-x[i*dimension+6]+x[i*dimension+10];
                    g9[i]=-2*x[i*dimension+7]-x[i*dimension+8]+x[i*dimension+11];

                    gg1[i]=0;
                    gg2[i]=0;
                    gg3[i]=0;
                    gg4[i]=0;
                    gg5[i]=0;
                    gg6[i]=0;
                    gg7[i]=0;
                    gg8[i]=0;
                    gg9[i]=0;
                    if (g1[i]>0) {
                    gg1[i]=g1[i];
                    }
                    if (g2[i]>0) {
                    gg2[i]=g2[i];
                    }
                    if (g3[i]>0) {
                    gg3[i]=g3[i];
                    }
                    if (g4[i]>0) {
                    gg4[i]=g4[i];
                    }
                    if (g5[i]>0) {
                    gg5[i]=g5[i];
                    }
                    if (g6[i]>0) {
                    gg6[i]=g6[i];
                    }
                    if (g7[i]>0) {
                    gg7[i]=g7[i];
                    }
                    if (g8[i]>0) {
                    gg8[i]=g8[i];
                    }
                    if (g9[i]>0) {
                    gg9[i]=g9[i];
                    }
                    gg[i]=gg1[i]+gg2[i]+gg3[i]+gg4[i]+gg5[i]+gg6[i]+gg7[i]+gg8[i]+gg9[i];

                    TAV[i]= gg[i] + glimites[i];
"""
