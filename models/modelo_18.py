import numpy as np

dimension = 9
time_max= 10  #tiempo maximo de conputo permitido
error=0.01 #CR3 se usa e para comparar si el CR3 es true o false

#Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10,10,10,10,10,10,10,10,20])

   
#Creo el vector limit_low_V, limite inferior   
limit_low_V = np.array([-10,-10,-10,-10,-10,-10,-10,-10,0])

solution_position_V=np.array([-0.657776192427943163,-0.153418773482438542,0.323413871675240938,-0.946257611651304398,-0.657776194376798906,-0.753213434632691414,0.323413874123576972,-0.346462947962331735,0.59979466285217542])
solution_global=-0.866025403784439


def function_serial(x,particles_number,limit_up_V,limit_low_V):
    glimitesuperior = np.zeros((particles_number,dimension))
    glimiteinferior = np.zeros((particles_number,dimension))
    gglimitesuperior = np.zeros((particles_number,dimension))
    gglimiteinferior = np.zeros((particles_number,dimension))
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i]=-0.5*(x[i][0]*x[i][3]-x[i][1]*x[i][2]+x[i][2]*x[i][8]-x[i][4]*x[i][8]+x[i][4]*x[i][7]-x[i][5]*x[i][6])
        g1=(x[i][2])**2+(x[i][3])**2-1
        g2=(x[i][8])**2-1
        g3=(x[i][4])**2+(x[i][5])**2-1
        g4=(x[i][0])**2+(x[i][1]-x[i][8])**2-1
        g5=(x[i][0]-x[i][4])**2+(x[i][1]-x[i][5])**2-1
        g6=(x[i][0]-x[i][6])**2+(x[i][1]-x[i][7])**2-1
        g7=(x[i][2]-x[i][4])**2+(x[i][3]-x[i][5])**2-1
        g8=(x[i][2]-x[i][6])**2+(x[i][3]-x[i][7])**2-1
        g9=(x[i][6])**2+(x[i][7]-x[i][8])**2-1
        g10=x[i][1]*x[i][2]-x[i][0]*x[i][3]
        g11=-x[i][2]*x[i][8]
        g12=x[i][4]*x[i][8]
        g13=x[i][5]*x[i][6]-x[i][4]*x[i][7]
        gg1=max(0,g1)
        gg2=max(0,g2)
        gg3=max(0,g3)
        gg4=max(0,g4)
        gg5=max(0,g5)
        gg6=max(0,g6)
        gg7=max(0,g7)
        gg8=max(0,g8)
        gg9=max(0,g9)
        gg10=max(0,g10)
        gg11=max(0,g11)
        gg12=max(0,g12)
        gg13=max(0,g13)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g=gg1+gg2+gg3+gg4+gg5+gg6+gg7+gg8+gg9+gg10+gg11+gg12+gg13
        TAV[i]=g+glimites[i]
    return f,TAV

function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float g1[particles_number];
            float g2[particles_number];
            float g3[particles_number];
            float g4[particles_number];
            float g5[particles_number];
            float g6[particles_number];
            float g7[particles_number];
            float g8[particles_number];
            float g9[particles_number];
            float g10[particles_number];
            float g11[particles_number];
            float g12[particles_number];
            float g13[particles_number];
            float gg1[particles_number];
            float gg2[particles_number];
            float gg3[particles_number];
            float gg4[particles_number];
            float gg5[particles_number];
            float gg6[particles_number];
            float gg7[particles_number];
            float gg8[particles_number];
            float gg9[particles_number];
            float gg10[particles_number];
            float gg11[particles_number];
            float gg12[particles_number];
            float gg13[particles_number];
            float gg[particles_number];

            f[i]= -0.5*(x[i*dimension+0]*x[i*dimension+3]-x[i*dimension+1]*x[i*dimension+2]+x[i*dimension+2]*x[i*dimension+8]-x[i*dimension+4]*x[i*dimension+8]+x[i*dimension+4]*x[i*dimension+7]-x[i*dimension+5]*x[i*dimension+6]);

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }


            g1[i]= pow((x[i*dimension+2]),2)+pow((x[i*dimension+3]),2)-1;
            g2[i]= pow((x[i*dimension+8]),2)-1;
            g3[i]= pow((x[i*dimension+4]),2)+pow((x[i*dimension+5]),2)-1;
            g4[i]= pow((x[i*dimension+0]),2)+pow((x[i*dimension+1]-x[i*dimension+8]),2)-1;
            g5[i]= pow((x[i*dimension+0]-x[i*dimension+4]),2)+pow((x[i*dimension+1]-x[i*dimension+5]),2)-1;
            g6[i]= pow((x[i*dimension+0]-x[i*dimension+6]),2)+pow((x[i*dimension+1]-x[i*dimension+7]),2)-1;
            g7[i]= pow((x[i*dimension+2]-x[i*dimension+4]),2)+pow((x[i*dimension+3]-x[i*dimension+5]),2)-1;
            g8[i]= pow((x[i*dimension+2]-x[i*dimension+6]),2)+pow((x[i*dimension+3]-x[i*dimension+7]),2)-1;
            g9[i]= pow((x[i*dimension+6]),2)+pow((x[i*dimension+7]-x[i*dimension+8]),2)-1;
            g10[i]= x[i*dimension+1]*x[i*dimension+2]-x[i*dimension+0]*x[i*dimension+3];
            g11[i]= -x[i*dimension+2]*x[i*dimension+8];
            g12[i]= x[i*dimension+4]*x[i*dimension+8];
            g13[i]= x[i*dimension+5]*x[i*dimension+6]-x[i*dimension+4]*x[i*dimension+7];

            gg1[i]=0;
            gg2[i]=0;
            gg3[i]=0;
            gg4[i]=0;
            gg5[i]=0;
            gg6[i]=0;
            gg7[i]=0;
            gg8[i]=0;
            gg9[i]=0;
            gg10[i]=0;
            gg11[i]=0;
            gg12[i]=0;
            gg13[i]=0;

            if (g1[i]>0) {
            gg1[i]=g1[i];
            }
            if (g2[i]>0) {
            gg2[i]=g2[i];
            }
            if (g3[i]>0) {
            gg3[i]=g3[i];
            }
            if (g4[i]>0) {
            gg4[i]=g4[i];
            }
            if (g5[i]>0) {
            gg5[i]=g5[i];
            }
            if (g6[i]>0) {
            gg6[i]=g6[i];
            }
            if (g7[i]>0) {
            gg7[i]=g7[i];
            }
            if (g8[i]>0) {
            gg8[i]=g8[i];
            }
            if (g9[i]>0) {
            gg9[i]=g9[i];
            }
            if (g10[i]>0) {
            gg10[i]=g10[i];
            }
            if (g11[i]>0) {
            gg11[i]=g11[i];
            }
            if (g12[i]>0) {
            gg12[i]=g12[i];
            }
            if (g13[i]>0) {
            gg13[i]=g13[i];
            }
            gg[i]=gg1[i]+gg2[i]+gg3[i]+gg4[i]+gg5[i]+gg6[i]+gg7[i]+gg8[i]+gg9[i]+gg10[i]+gg11[i]+gg12[i]+gg13[i];

            TAV[i]=glimites[i]+gg[i];
"""

