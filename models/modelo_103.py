import numpy as np

dimension = 2
time_max= 10  #tiempo maximo de conputo permitido
error=0.000001 #CR3 se usa e para comparar si el CR3 es true o false

#Creo el vector limit_up_V, limite superior
limit_up_V = np.array([np.pi,np.pi])

#Creo el vector xlo, limite inferior   
limit_low_V = np.array([0,0])


solution_position_V=np.array([2.2,1.57])
solution_global=-1.8013


def function_serial(x, particles_number, limit_up_V, limit_low_V):   #Michalewics Function
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        s = 0
        for j in range(dimension):
            s=s+np.sin(x[i][j])*(np.sin(((j+1)*(x[i][j])**2)/np.pi))**20
        f[i]=-s
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        TAV[i]=glimites[i]
    return f,TAV

function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float suma[particles_number];

            suma[i] = 0;
            for (int jjj=0; jjj < dimension; jjj++){
                suma[i]=suma[i]+sin(x[i * dimension+jjj]) * pow(sin((jjj+1) * x[i * dimension+jjj] * x[i * dimension+jjj] / M_PI), 20);
            }
            f[i] = -suma[i];

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            TAV[i]=glimites[i];
"""
