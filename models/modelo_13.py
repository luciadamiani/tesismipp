import numpy as np

dimension = 5
time_max = 10  # tiempo maximo de conputo permitido
error = 0.0000001  # CR3 se usa error para comparar si el CR3 es true o false

# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([2.3, 2.3, 3.2, 3.2, 3.2])

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([-2.3, -2.3, -3.2, -3.2, -3.2])

solution_position_V = np.array([-1.71714224003, 1.59572124049468, 1.8272502406271, -0.763659881912867, -0.76365986736498])
solution_global = 0.053941514041898


def function_serial(x, particles_number, limit_up_V, limit_low_V):
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i] = np.exp(x[i][0] * x[i][1] * x[i][2] * x[i][3] * x[i][4])
        h1 = (x[i][0]) ** 2 + (x[i][1]) ** 2 + (x[i][2]) ** 2 + (x[i][3]) ** 2 + (x[i][4]) ** 2 - 10
        h2 = x[i][1] * x[i][2] - 5 * x[i][3] * x[i][4]
        h3 = (x[i][0]) ** 3 + (x[i][1]) ** 3 + 1
        h = abs(h1) + abs(h2) + abs(h3)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        TAV[i] = h + glimites[i]
    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float h1[particles_number];
            float h2[particles_number];
            float h3[particles_number];
            float h[particles_number];

            f[i]= exp(x[i*dimension+0]*x[i*dimension+1]*x[i*dimension+2]*x[i*dimension+3]*x[i*dimension+4]);

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            h1[i]= pow((x[i*dimension+0]),2)+pow((x[i*dimension+1]),2)+pow((x[i*dimension+2]),2)+pow((x[i*dimension+3]),2)+pow((x[i*dimension+4]),2)-10;
            h2[i]= x[i*dimension+1]*x[i*dimension+2]-5*x[i*dimension+3]*x[i*dimension+4];
            h3[i]= pow((x[i*dimension+0]),3)+pow((x[i*dimension+1]),3)+1;
            h[i]= abs(h1[i])+abs(h2[i])+abs(h3[i]);

            TAV[i]=glimites[i]+h[i];
"""
