import numpy as np

dimension = 10
time_max = 10  # tiempo maximo de conputo permitido
error = 0.000000001  # CR3 se usa error para comparar si el CR3 es true o false
RG = 10  # se usa para el CR3: promedio de fg tomados
limit_up_V = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])  # Creo el vector limit_up_V, limite superior
limit_low_V = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])  # Creo el vector limit_low_V, limite inferior

solution_position_V = np.array([0.31624357647283069] * dimension)
solution_global = -1.00050010001000


def function_serial(x, particles_number, limit_up_V, limit_low_V):  # function_id_serial go3  -->comprobado q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        pp = 1
        ss = 0
        for jjj in range(dimension):
            pp = pp * x[i][jjj]
            ss = ss + np.power(x[i][jjj], 2)
        f[i] = -(np.power(np.sqrt(10), dimension)) * pp
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        h = np.abs(ss - 1)
        TAV[i] = h + glimites[i]
    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float ss[particles_number];
            float pp[particles_number];
            float h[particles_number];

            pp[i]=1;
            ss[i]=0;
            for (int jjj=0; jjj<dimension; jjj++){
                pp[i]=pp[i]*x[i*dimension+jjj];
                ss[i]=ss[i]+pow(x[i*dimension+jjj],2);
            }

            f[i]=-(pow(sqrt((float)dimension),dimension))*pp[i];

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            h[i]= abs(ss[i]-1);

            TAV[i] = h[i] + glimites[i];
"""




# ///////////////////calculo de la function_id_serial: en este caso modelo g03/////////////////////////////////
#
# float pp=1;
# float ss=0;
# for (int jjj=0; jjj<dimension; jjj++){
#     pp=pp*x[i*dimension+j];
#     ss=ss+pow(x[i*dimension+j],2);
# }
# f[i]=-(pow(sqrt((float)dimension),dimension))*pp;
# float h= abs(ss-1);
# TAV[i]=h;
#
# ////////////////////////////////////////////////////////////////////////////////////////////////////
