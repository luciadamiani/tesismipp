import numpy as np

dimension = 8
time_max= 2  #tiempo maximo de conputo permitido
error=0.01 #CR3 se usa e para comparar si el CR3 es true o false

#Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10000,10000,10000,1000,1000,1000,1000,1000])
   
#Creo el vector limit_low_V, limite inferior   
limit_low_V = np.array([100,1000,1000,10,10,10,10,10])

solution_position_V=np.array([579.306685017979589,1359.97067807935605,5109.97065743133317,182.01769963061534,295.601173702746792,217.982300369384632,286.41652592786852,395.601173702746735])
solution_global=7049.24802052867


def function_serial(x,particles_number,limit_up_V,limit_low_V): #funcion g10 --> comprobe q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number,dimension))
    glimiteinferior = np.zeros((particles_number,dimension))
    gglimitesuperior = np.zeros((particles_number,dimension))
    gglimiteinferior = np.zeros((particles_number,dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i]=x[i][0]+x[i][1]+x[i][2]
        g1=-1+0.0025*(x[i][3]+x[i][5])
        g2=-1+0.0025*(x[i][4]+x[i][6]-x[i][3])
        g3=-1+0.01*(x[i][7]-x[i][4])
        g4=-x[i][0]*x[i][5]+833.33252*x[i][3]+100*x[i][0]-83333.333
        g5=-x[i][1]*x[i][6]+1250*x[i][4]+x[i][1]*x[i][3]-1250*x[i][3]
        g6=-x[i][2]*x[i][7]+1250000+x[i][2]*x[i][4]-2500*x[i][4]
        gg1=max(0,g1)
        gg2=max(0,g2)
        gg3=max(0,g3)
        gg4=max(0,g4)
        gg5=max(0,g5)
        gg6=max(0,g6)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g=gg1+gg2+gg3+gg4+gg5+gg6
        TAV[i]=g+glimites[i]
    return f,TAV

function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float g1[particles_number];
            float g2[particles_number];
            float g3[particles_number];
            float g4[particles_number];
            float g5[particles_number];
            float g6[particles_number];
            float gg1[particles_number];
            float gg2[particles_number];
            float gg3[particles_number];
            float gg4[particles_number];
            float gg5[particles_number];
            float gg6[particles_number];
            float gg[particles_number];

            f[i]= x[i*dimension+0]+x[i*dimension+1]+x[i*dimension+2];

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            g1[i]= -1+0.0025*(x[i*dimension+3]+x[i*dimension+5]);
            g2[i]= -1+0.0025*(x[i*dimension+4]+x[i*dimension+6]-x[i*dimension+3]);
            g3[i] = -1+0.01*(x[i*dimension+7]-x[i*dimension+4]);
            g4[i]= -x[i*dimension+0]*x[i*dimension+5]+833.33252*x[i*dimension+3]+100*x[i*dimension+0]-83333.333;
            g5[i]= -x[i*dimension+1]*x[i*dimension+6]+1250*x[i*dimension+4]+x[i*dimension+1]*x[i*dimension+3]-1250*x[i*dimension+3];
            g6[i]= -x[i*dimension+2]*x[i*dimension+7]+1250000+x[i*dimension+2]*x[i*dimension+4]-2500*x[i*dimension+4];

            gg1[i]=0;
            gg2[i]=0;
            gg3[i] =0;
            gg4[i]=0;
            gg5[i]=0;
            gg6[i]=0;

            if (g1[i]>0) {
                gg1[i]=g1[i];
            }
            if (g2[i]>0) {
                gg2[i]=g2[i];
            }
            if (g3[i] >0) {
                gg3[i] =g3[i] ;
            }
            if (g4[i]>0) {
                gg4[i]=g4[i];
            }
            if (g5[i]>0) {
                gg5[i]=g5[i];
            }
            if (g6[i]>0) {
                gg6[i]=g6[i];
            }

            gg[i]=gg1[i]+gg2[i]+gg3[i] +gg4[i]+gg5[i]+gg6[i];

            TAV[i]=glimites[i]+gg[i];
"""
