import numpy as np

dimension = 5
time_max = 10  # tiempo maximo de conputo permitido
error = 0.01  # CR3 se usa error para comparar si el CR3 es true o false
# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([102, 45, 45, 45, 45])

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([78, 33, 27, 27, 27])

solution_position_V = np.array([78, 33, 29.9952560256815985, 45, 36.7758129057882073])
solution_global = -3.066553867178332e4


def function_serial(x, particles_number, limit_up_V, limit_low_V):  # funcion go4    --> controlada q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i] = 5.3578547 * x[i][2] ** 2 + 0.8356891 * x[i][0] * x[i][4] + 37.293239 * x[i][0] - 40792.141
        g1 = 85.334407 + 0.0056858 * x[i][1] * x[i][4] + 0.0006262 * x[i][0] * x[i][3] - 0.0022053 * x[i][2] * x[i][4] - 92
        g2 = -85.334407 - 0.0056858 * x[i][1] * x[i][4] - 0.0006262 * x[i][0] * x[i][3] + 0.0022053 * x[i][2] * x[i][4]
        g3 = 80.51249 + 0.0071317 * x[i][1] * x[i][4] + 0.0029955 * x[i][0] * x[i][1] + 0.0021813 * (x[i][2]) ** 2 - 110
        g4 = -80.51249 - 0.0071317 * x[i][1] * x[i][4] - 0.0029955 * x[i][0] * x[i][1] - 0.0021813 * (x[i][2]) ** 2 + 90
        g5 = 9.300961 + 0.0047026 * x[i][2] * x[i][4] + 0.0012547 * x[i][0] * x[i][2] + 0.0019085 * x[i][2] * x[i][
            3] - 25
        g6 = -9.300961 - 0.0047026 * x[i][2] * x[i][4] - 0.0012547 * x[i][0] * x[i][2] - 0.0019085 * x[i][2] * x[i][
            3] + 20
        gg1 = max(0, g1)
        gg2 = max(0, g2)
        gg3 = max(0, g3)
        gg4 = max(0, g4)
        gg5 = max(0, g5)
        gg6 = max(0, g6)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g = gg1 + gg2 + gg3 + gg4 + gg5 + gg6
        TAV[i] = g + glimites[i]
    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float g1[particles_number];
            float g2[particles_number];
            float g3[particles_number];
            float g4[particles_number];
            float g5[particles_number];
            float g6[particles_number];
            float gg1[particles_number];
            float gg2[particles_number];
            float gg3[particles_number];
            float gg4[particles_number];
            float gg5[particles_number];
            float gg6[particles_number];
            float gg[particles_number];

            f[i]=5.3578547*pow(x[i*dimension+2],2) + 0.8356891*x[i*dimension+0]*x[i*dimension+4] + 37.293239*x[i*dimension+0]-40792.141;

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }


            g1[i]= 85.334407 + 0.0056858*x[i*dimension+1]*x[i*dimension+4] + 0.0006262*x[i*dimension+0]*x[i*dimension+3] - 0.0022053*x[i*dimension+2]*x[i*dimension+4]-92;
            g2[i]=-85.334407 - 0.0056858*x[i*dimension+1]*x[i*dimension+4] - 0.0006262*x[i*dimension+0]*x[i*dimension+3] + 0.0022053*x[i*dimension+2]*x[i*dimension+4];
            g3[i]=80.51249 + 0.0071317*x[i*dimension+1]*x[i*dimension+4] + 0.0029955*x[i*dimension+0]*x[i*dimension+1] + 0.0021813*pow(x[i*dimension+2],2)-110;
            g4[i]=-80.51249 - 0.0071317*x[i*dimension+1]*x[i*dimension+4] - 0.0029955*x[i*dimension+0]*x[i*dimension+1]- 0.0021813*pow(x[i*dimension+2],2) + 90;
            g5[i]= 9.300961 + 0.0047026*x[i*dimension+2]*x[i*dimension+4] + 0.0012547*x[i*dimension+0]*x[i*dimension+2] + 0.0019085*x[i*dimension+2]*x[i*dimension+3]- 25;
            g6[i]=-9.300961 - 0.0047026*x[i*dimension+2]*x[i*dimension+4] - 0.0012547*x[i*dimension+0]*x[i*dimension+2] - 0.0019085*x[i*dimension+2]*x[i*dimension+3] + 20;

            gg1[i]=0;
            gg2[i]=0;
            gg3[i]=0;
            gg4[i]=0;
            gg5[i]=0;
            gg6[i]=0;

            if (g1[i]>0) {
                gg1[i]=g1[i];
            }
            if (g2[i]>0) {
                gg2[i]=g2[i];
            }
            if (g3[i]>0) {
                gg3[i]=g3[i];
            }
            if (g4[i]>0) {
                gg4[i]=g4[i];
            }
            if (g5[i]>0) {
                gg5[i]=g5[i];
            }
            if (g6[i]>0) {
                gg6[i]=g6[i];
            }

            gg[i]=gg1[i]+gg2[i]+gg3[i]+gg4[i]+gg5[i]+gg6[i];

            TAV[i]=glimites[i]+gg[i];
"""
