import numpy as np

dimension = 2
time_max = 10  # tiempo maximo de conputo permitido
error = 0.01  # CR3 se usa e para comparar si el CR3 es true o false

# Creo el vector limit_up_V, limite superior
limit_up_V = np.array([1, 1])

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([-1, -1])

solution_position_V = np.array([-0.707036070037170616, 0.500000004333606807])
solution_global = 0.7499


def function_serial(x, particles_number, limit_up_V, limit_low_V):  # funcion g11 --> comprobe q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i] = (x[i][0]) ** 2 + (x[i][1] - 1) ** 2
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        h = abs(x[i][1] - (x[i][0]) ** 2)
        TAV[i] = h + glimites[i]
    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float h[particles_number];

            f[i]=pow((x[i*dimension+0]),2)+pow((x[i*dimension+1]-1),2);

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            h[i]=abs(x[i*dimension+1]-pow((x[i*dimension+0]),2));

            TAV[i]=glimites[i]+h[i];
"""
