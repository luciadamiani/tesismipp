import numpy as np

dimension = 4
time_max = 10  # tiempo maximo de conputo permitido
error = 0.01  # CR3 se usa e para comparar si el CR3 es true o false

# Creo el vector xup, limite superior
limit_up_V = np.array([1200, 1200, 0.55, 0.55])

# Creo el vector limit_low_V, limite inferior
limit_low_V = np.array([0, 0, -0.55, -0.55])

solution_position_V = np.array([679.945148297028709, 1026.06697600004691, 0.118876369094410433, -0.39623348521517826])
solution_global = 5126.4967140071


def function_serial(x, particles_number, limit_up_V, limit_low_V):  # funcion g05 --> comprobe q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number, dimension))
    glimiteinferior = np.zeros((particles_number, dimension))
    gglimitesuperior = np.zeros((particles_number, dimension))
    gglimiteinferior = np.zeros((particles_number, dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i] = 3 * x[i][0] + 0.000001 * (x[i][0]) ** 3 + 2 * x[i][1] + (0.000002 / 3) * (x[i][1]) ** 3
        h1 = 1000 * np.sin(-x[i][2] - 0.25) + 1000 * np.sin(-x[i][3] - 0.25) + 894.8 - x[i][0]
        h2 = 1000 * np.sin(x[i][2] - 0.25) + 1000 * np.sin(x[i][2] - x[i][3] - 0.25) + 894.8 - x[i][1]
        h3 = 1000 * np.sin(x[i][3] - 0.25) + 1000 * np.sin(x[i][3] - x[i][2] - 0.25) + 1294.8
        h = abs(h1) + abs(h2) + abs(h3)
        g1 = -x[i][3] + x[i][2] - 0.55
        g2 = -x[i][2] + x[i][3] - 0.55
        gg1 = max(0, g1)
        gg2 = max(0, g2)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g = gg1 + gg2
        TAV[i] = h + g + glimites[i]
    return f, TAV


function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float g1[particles_number];
            float g2[particles_number];
            float gg1[particles_number];
            float gg2[particles_number];
            float gg[particles_number];
            float h1[particles_number];
            float h2[particles_number];
            float h3[particles_number];
            float h[particles_number];

            f[i]=3*x[i*dimension+0] + 0.000001*pow(x[i*dimension+0],3)+2*x[i*dimension+1] + (0.000002/3)*pow(x[i*dimension+1],3);

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            h1[i]=1000*sin(-x[i*dimension+2] - 0.25) + 1000*sin(-x[i*dimension+3] - 0.25) + 894.8 - x[i*dimension+0];
            h2[i]=1000*sin(x[i*dimension+2]- 0.25) + 1000*sin(x[i*dimension+2]-x[i*dimension+3] - 0.25) + 894.8 - x[i*dimension+1];
            h3[i]=1000*sin(x[i*dimension+3] - 0.25) + 1000*sin(x[i*dimension+3]-x[i*dimension+2]- 0.25) + 1294.8;
            h[i]=abs(h1[i])+abs(h2[i])+abs(h3[i]);

            g1[i]=-x[i*dimension+3] + x[i*dimension+2] - 0.55;
            g2[i]=-x[i*dimension+2] + x[i*dimension+3] - 0.55;

            gg1[i]=0;
            gg2[i]=0;

            if (g1[i]>0) {
            gg1[i]=g1[i];
            }
            if (g2[i]>0) {
            gg2[i]=g2[i];
            }

            gg[i]=gg1[i]+gg2[i];
            TAV[i]=h[i]+gg[i]+glimites[i];
"""
