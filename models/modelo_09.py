import numpy as np

dimension = 7
time_max= 10  #tiempo maximo de conputo permitido
error=0.01 #CR3 se usa error para comparar si el CR3 es true o false

#Creo el vector limit_up_V, limite superior
limit_up_V = np.array([10,10,10,10,10,10,10])
   
#Creo el vector limit_low_V, limite inferior   
limit_low_V = np.array([-10,-10,-10,-10,-10,-10,-10])

solution_position_V=np.array([2.33049935147405174,1.95137236847114592,-0.477541399510615805,4.36572624923625874,-0.624486959100388983,1.03813099410962173,1.5942266780671519])
solution_global=680.630057374402


def function_serial(x,particles_number,limit_up_V,limit_low_V): #funcion g09 --> comprobe q hace lo q quiero
    f = np.zeros(particles_number)
    TAV = np.zeros(particles_number)
    glimitesuperior = np.zeros((particles_number,dimension))
    glimiteinferior = np.zeros((particles_number,dimension))
    gglimitesuperior = np.zeros((particles_number,dimension))
    gglimiteinferior = np.zeros((particles_number,dimension))
    glimites = np.zeros(particles_number)
    for i in range(particles_number):
        f[i]=(x[i][0]-10)**2+5*(x[i][1]-12)**2+(x[i][2])**4+3*(x[i][3]-11)**2+10*(x[i][4])**6+7*(x[i][5])**2+(x[i][6])**4-4*x[i][5]*x[i][6]-10*x[i][5]-8*x[i][6]
        g1= -127 + 2*(x[i][0])**2+3*(x[i][1])**4+x[i][2]+4*(x[i][3])**2+5*x[i][4]
        g2= -282 + 7*x[i][0]+3*x[i][1]+10*(x[i][2])**2+x[i][3]-x[i][4]
        g3= -196 + 23*x[i][0]+(x[i][1])**2+6*(x[i][5])**2-8*x[i][6]
        g4= 4*(x[i][0])**2+(x[i][1])**2-3*x[i][0]*x[i][1]+2*(x[i][2])**2+5*x[i][5]-11*x[i][6]
        gg1=max(0,g1)
        gg2=max(0,g2)
        gg3=max(0,g3)
        gg4=max(0,g4)
        glimites[i] = 0
        for j in range(dimension):
            glimitesuperior[i][j] = x[i][j] - limit_up_V[j]
            glimiteinferior[i][j] = limit_low_V[j] - x[i][j]
            gglimitesuperior[i][j] = max(0, glimitesuperior[i][j])
            gglimiteinferior[i][j] = max(0, glimiteinferior[i][j])
            glimites[i] = glimites[i] + gglimitesuperior[i][j] + gglimiteinferior[i][j]
        g=gg1+gg2+gg3+gg4
        TAV[i]=g+glimites[i]
    return f,TAV

function_parallel = """
            float glimitesuperior[particles_number*dimension+dimension];
            float glimiteinferior[particles_number*dimension+dimension];
            float glimites[particles_number];
            float g1[particles_number];
            float g2[particles_number];
            float g3[particles_number];
            float g4[particles_number];
            float gg1[particles_number];
            float gg2[particles_number];
            float gg3[particles_number];
            float gg4[particles_number];
            float gg[particles_number];

            f[i]= pow((x[i*dimension+0]-10),2) +5*pow((x[i*dimension+1]-12),2)+pow((x[i*dimension+2]),4)+3*pow((x[i*dimension+3]-11),2)+10*pow((x[i*dimension+4]),6)+7*pow((x[i*dimension+5]),2)+pow((x[i*dimension+6]),4)-4*x[i*dimension+5]*x[i*dimension+6]-10*x[i*dimension+5]-8*x[i*dimension+6];

            glimites[i]=0;

            for (int jjj=0; jjj<dimension; jjj++){
                glimitesuperior[i*dimension+jjj] = x[i*dimension+jjj] - limit_up_V[jjj];
                glimiteinferior[i*dimension+jjj] = limit_low_V[jjj] - x[i*dimension+jjj];

                if (glimitesuperior[i*dimension+jjj]<0) {

                glimitesuperior[i*dimension+jjj]=0;

                }

                if (glimiteinferior[i*dimension+jjj]<0) {

                glimiteinferior[i*dimension+jjj]=0;

                }

                glimites[i]=glimites[i]+glimiteinferior[i*dimension+jjj]+glimitesuperior[i*dimension+jjj];
            }

            g1[i]= -127 + 2*pow((x[i*dimension+0]),2)+3*pow((x[i*dimension+1]),4)+x[i*dimension+2]+4*pow((x[i*dimension+3]),2)+5*x[i*dimension+4];
            g2[i]= -282 + 7*x[i*dimension+0]+3*x[i*dimension+1]+10*pow((x[i*dimension+2]),2)+x[i*dimension+3]-x[i*dimension+4];
            g3[i]= -196 + 23*x[i*dimension+0]+pow((x[i*dimension+1]),2)+6*pow((x[i*dimension+5]),2)-8*x[i*dimension+6];
            g4[i]= 4*pow((x[i*dimension+0]),2)+pow((x[i*dimension+1]),2)-3*x[i*dimension+0]*x[i*dimension+1]+2*pow((x[i*dimension+2]),2)+5*x[i*dimension+5]-11*x[i*dimension+6];

            gg1[i]=0;
            gg2[i]=0;
            gg3[i]=0;
            gg4[i]=0;

            if (g1[i]>0) {
                gg1[i]=g1[i];
            }
            if (g2[i]>0) {
                gg2[i]=g2[i];
            }
            if (g3[i]>0) {
                gg3[i]=g3[i];
            }
            if (g4[i]>0) {
                gg4[i]=g4[i];
            }

            gg[i]=gg1[i]+gg2[i]+gg3[i]+gg4[i];

            TAV[i]=glimites[i]+gg[i];
"""