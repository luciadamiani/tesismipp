
import numpy as np
# libreria que uso para poder paralelizar
import pycuda.driver as pycuda

from common.results import Results
from pycuda import compiler

BUSCAR = "//funcion objetivo"

kernel_modelo = """
    #include <stdio.h>
    #include <math.h>
    __global__ void kernel_pso_parallel(float *u_final, float *TAVG_final, float *error_final, float *g_final,float *fg_final, float *v, float *r1, float *x, float *r2, float *g, float *limit_up_V, float *limit_low_V, float *fg, float *vmax, float *fgreal, float *TAV, float TAVmin, float *TAVG, float *u, float *f)
    {
        const int i = blockDim.x * blockIdx.x + threadIdx.x;
        const int j = blockDim.y * blockIdx.y + threadIdx.y;

        //CONSTANTES
        const int particles_number=%(num_part)s;
        const int dimension=%(dimension)s;
        const float c1=%(c1)s;
        const float c2=%(c2)s;
        const float w=%(w)s;
        const int iteraciones_maximas=%(iteraciones_maximas)s;

        //MEMORIA COMPARTIDA
        __shared__ float v_s[particles_number*dimension];
        __shared__ float x_s[particles_number*dimension];
        __shared__ float v_final_s[particles_number*dimension];
        __shared__ float x_final_s[particles_number*dimension];
        __shared__ float vmax_s[dimension];
        __shared__ float fgreal_s[1];
        __shared__ float u_s[1];
        __shared__ float u_final_s[1];
        __shared__ float f_s[particles_number];
        __shared__ float fp_final_s[particles_number];
        __shared__ float p_final_s[particles_number*dimension];
        __shared__ float TAVP_final_s[particles_number];
        __shared__ float error_s[1];
        __shared__ float g_s[dimension];
        __shared__ float TAVG_s[1];
        __shared__ float fg_s[1];
        __shared__ float TAV_s[particles_number];

        //ASIGNACION DE MEMORIA GLOBAL A MEMORIA COMPARTIDA
        v_s[ i*dimension + j]=v[ i*dimension + j];
        x_s[ i*dimension + j]=x[ i*dimension + j];
        v_final_s[ i*dimension + j]=0;
        x_final_s[ i*dimension + j]=0;
        vmax_s[j]=vmax[j];
        fgreal_s[0]=fgreal[0];
        u_s[0]=u[0];
        f_s[i]=f[i];
        fp_final_s[i] = f[i];
        p_final_s[i*dimension+j] = x[i*dimension+j];
        TAVP_final_s[i] = TAV_s[i];
        fg_s[0] =  fg[0];
        TAVG_s[0]= TAVG[0];
        g_s[j]= g[j];


        int CR1=1;
        int k=0;
        __syncthreads();

        while(CR1==1){

            v_final_s[ i*dimension + j] =  w * v_s[ i*dimension + j] + c1 * r1[ i*dimension + j + k*dimension*particles_number] * ( p_final_s[ i*dimension + j ] - x_s[ i*dimension + j] ) + c2 * r2[ i*dimension + j + k*dimension*particles_number] * (g_s[ j ] - x_s[ i*dimension + j]);

            __syncthreads();

            if (v_final_s[ i*dimension + j ]>abs(vmax_s[j])){
                v_final_s[ i*dimension + j ] = abs(vmax_s[j]);
            }

            if (v_final_s[ i*dimension + j ]<-abs(vmax_s[j])){
                v_final_s[ i*dimension + j ] = -abs(vmax_s[j]);
            }

            v_s[ i*dimension + j] = v_final_s[ i*dimension + j ];

            __syncthreads();


            x_final_s[ i*dimension + j ] = x_s[ i*dimension + j] + v_final_s[ i*dimension + j ] ;

            __syncthreads();

            x_s[ i*dimension + j]=x_final_s[i*dimension+j];
            __syncthreads();


            ///////////////////calculo de la funcion de cada modelo en particular /////////////////////////////////

            //funcion objetivo

            ////////////////////////////////////////////////////////////////////////////////////////////////////

            k=k+1;
            __syncthreads();

            //-----------------busqueda del mejor local y global----------------------------------------------------------------------------------------
            if (i == 0 and j==0){
                if (k==1){
                    float sumaTAV=0;
                    for (int aa=0; aa < particles_number; aa++){
                        sumaTAV=sumaTAV+TAV_s[aa];
                    }
                    u_s[i]=sumaTAV/particles_number;
                }

                else{
                    int ff=0;  //sirve para la actualizacion de u
                    for (int aa=0; aa < particles_number; aa++){
                        if (TAV_s[aa]<=u_s[i]){
                            ff=ff+1;
                        }
                    }
                    int FF=ff;

                    u_final_s[i]=u_s[i]*(1-(FF*1.0/particles_number));

                    u_s[i]=u_final_s[i];
                }
            }
            __syncthreads();

            if ((TAV_s[i]<=u_s[0]) & (TAVP_final_s[i]>u_s[0])){
                fp_final_s[i] = f_s[i];
                TAVP_final_s[i] = TAV_s[i];
                p_final_s[i*dimension+j] = x_s[ i*dimension + j];
            }
            if ((TAV_s[i]<=u_s[0]) & (TAVP_final_s[i]<=u_s[0]) & (f_s[i]<=fp_final_s[i])){
                fp_final_s[i] = f_s[i];
                TAVP_final_s[i] = TAV_s[i];
                p_final_s[i*dimension+j] = x_s[ i*dimension + j];
            }


            if ((TAV_s[i]>u_s[0]) & (TAVP_final_s[i]>u_s[0]) & (TAV_s[i]<=TAVP_final_s[i])){
                fp_final_s[i] = f_s[i];
                TAVP_final_s[i] = TAV_s[i];
                p_final_s[i*dimension+j] = x_s[ i*dimension + j];
            }

            __syncthreads();

            //MEJOR GLOBAL EN SERIE (PARA BORRAR CUANDO SE CONFIRME QUE ES IGUAL A LO ANTERIOR)
            if (i == 0 and j==0){

                for (int aa=0; aa < particles_number; aa++){

                    if (TAV_s[aa]<=u_s[i] and TAVG_s[i]>u_s[i]){
                        fg_s[i] = f_s[aa];
                        TAVG_s[i]= TAV_s[aa];
                        for (int bb=0; bb<dimension; bb++){
                            g_s[bb]= x_s[aa*dimension+bb];
                        }
                    }
                    if (TAV_s[aa]<=u_s[i] and TAVG_s[i]<=u_s[i]){
                        if (f_s[aa]<=fg_s[i]){
                            fg_s[i] = f_s[aa];
                            TAVG_s[i]= TAV_s[aa];
                            for (int bb=0; bb<dimension; bb++){
                                g_s[bb]= x_s[aa*dimension+bb];
                            }
                        }
                    }

                    if (TAV_s[aa]>u_s[i] and TAVG_s[i]>u_s[i]){
                        if (TAV_s[aa]<=TAVG_s[i]){
                            fg_s[i] = f_s[aa];
                            TAVG_s[i]= TAV_s[aa];
                            for (int bb=0; bb<dimension; bb++){
                                g_s[bb]= x_s[aa*dimension+bb];
                            }
                        }
                    }


                }  //fin lazo a<particles_number
            }  //fin if(i==0)
            __syncthreads();

            /////HASTA ACA MEJOR GLOBAL EN SERIE, SE BORRARIA////



            if (k>=iteraciones_maximas){
                CR1=0;
            }

            if (fgreal_s[0]==0){
                error_s[0]=abs(fgreal_s[0]-fg_s[0]);
            }
            if (fgreal_s[0]!=0){
                error_s[0]=abs(fgreal_s[0]-fg_s[0])/abs(fgreal_s[0]);
            }

        }//FIN lazo while

        __syncthreads();

        if (i==0){
            fg_final[i] =  fg_s[i];
            TAVG_final[i]=TAVG_s[i];
            g_final[j]= g_s[j];
            u_final[i]=u_final_s[i];
            error_final[i]=error_s[i];
        }
    }//FIN kernel

"""


class PSOParallel:
    def __init__(self, user_parameters, model_parameters):
        kernel_code = kernel_modelo.replace(BUSCAR, model_parameters.function_parallel)
        kernel_code = kernel_code % {'num_part': user_parameters.particles_number,
                                     'dimension': model_parameters.dimension,
                                     'c1': user_parameters.c1, 'c2': user_parameters.c2, 'w': user_parameters.w,
                                     'iteraciones_maximas': user_parameters.iterations_max}

        kernel_code = compiler.SourceModule(kernel_code)
        self.kernel_compilado = kernel_code.get_function("kernel_pso_parallel")
        self.user_parameters = user_parameters
        self.model_parameters = model_parameters

    def compute(self, initial_values):
        v = (initial_values.v_initial).astype(np.float32)
        r1 = (initial_values.r1).astype(np.float32)
        x = (initial_values.x_initial).astype(np.float32)
        r2 = (initial_values.r2).astype(np.float32)
        g = (initial_values.g_initial).astype(np.float32)

        self.model_parameters.limit_up_V = (self.model_parameters.limit_up_V).astype(np.float32)
        self.model_parameters.limit_low_V = (self.model_parameters.limit_low_V).astype(np.float32)
        f = (initial_values.f_initial).astype(np.float32)
        TAV = (initial_values.TAV_initial).astype(np.float32)
        fg = np.array(initial_values.fg_initial).astype(np.float32)
        fg_final = np.array(0).astype(np.float32)
        g_final = (g).astype(np.float32)
        v_maxinicial = (initial_values.v_max_initial).astype(np.float32)
        solution_global_V = np.array(self.model_parameters.solution_global).astype(np.float32)
        error_final = np.array(0).astype(np.float32)
        u = np.array(initial_values.u_initial).astype(np.float32)

        f = (initial_values.f_initial).astype(np.float32)

        TAVmin = np.float32(999999999999)
        TAVG = np.array(initial_values.TAVG_initial).astype(np.float32)
        TAVG_final = np.array(99999).astype(np.float32)
        u_final = np.zeros_like(u).astype(np.float32)

        self.kernel_compilado(pycuda.Out(u_final), pycuda.Out(TAVG_final),
                            pycuda.Out(error_final),
                            pycuda.Out(g_final), pycuda.Out(fg_final),
                            pycuda.In(v), pycuda.In(r1), pycuda.In(x), pycuda.In(r2), pycuda.In(g),
                            pycuda.In(self.model_parameters.limit_up_V), pycuda.In(self.model_parameters.limit_low_V),
                            pycuda.In(fg), pycuda.In(v_maxinicial),
                            pycuda.In(solution_global_V), pycuda.In(TAV), np.float32(TAVmin), pycuda.In(TAVG),
                            pycuda.In(u), pycuda.In(f),
                            block=(self.user_parameters.particles_number, self.model_parameters.dimension,1),
                            grid=(1, 1, 1),
                            )

        parallel_results = Results(fg_final, error_final, TAVG_final, g_final, u_final)
        return parallel_results
