import numpy as np
import random
from common.results import Results


class PSOSerial:

    def __init__(self, user_parameters, model_parameters):
        self.user_parameters = user_parameters
        self.model_parameters = model_parameters

    # diferentes formulas para obtener w, segun bibliografia
    def formula_w(self, choice_w, p, g, k):
        if choice_w == 1:
            w = self.user_parameters.w  # tomo el w ingresado por teclado, es el que uso si opcionw=1
        elif choice_w == 2:
            w = 1.2
            w = 0.99 * w
        elif choice_w == 3:
            r = random.uniform(0, 1)
            w = 0.5 + r / 2
        elif choice_w == 4:
            wmax = 0.9
            wmin = 0.4
            t = k
            time_max = self.user_parameters.iterations_max
            w = wmax - (wmax - wmin) * t / time_max
        elif choice_w == 5:
            w = 1.1 - p / g
        elif choice_w == 6:
            r1 = random.uniform(0, 1)
            r2 = random.uniform(0, 1)
            z = 4 * r2 * (1 - r2)
            w = 0.5 * r1 + 0.5 * z
        return w

    # diferentes formas de que el parametro "u" se vaya reduciendo a medida que se recorre el lazo. Es function_serial de la
    # cantidad de particulas factibles (FF) y la cantidad total de particulas
    def formula_u(self, opcionu, u, FF, particles_number):
        if opcionu == 1:
            u = u * (1 - (FF * 1.0 / particles_number))  # recomendada por el paper
        if opcionu == 2:
            u = u * (1 - (
                    FF * 1.0 / particles_number)) ** 2  # se hace FF "*1.0" para que me devuelva un valor con coma, sino solo me devuelve la parte entera
        if opcionu == 3:
            if FF / particles_number >= 0 and FF / particles_number <= 0.9:
                u = u * (1 - FF)
            if FF > 0.9:
                u = 0.1 * u
        return u

    def compute(self, initial_values):
        # valores iniciales provenientes de otra funcion para comparar serie y paralelo
        p = initial_values.p_initial
        fp = initial_values.f_initial
        TAVP = initial_values.TAV_initial
        g = initial_values.g_initial
        fg = initial_values.fg_initial
        TAVG = initial_values.TAVG_initial
        u = initial_values.u_initial
        x = initial_values.x_initial
        v = initial_values.v_initial

        a = 0  # para que entre al "if" que me dice cuando u<0.001 solo una vez
        # b = constante que posteriormente va a indicar cuando el error entre fg y fg real es <0.01
        b = 0  # para que entre al "if" que me reporta la primer solucion global solo una vez

        # definiciones logicas de los criterios de terminacion para poder ingresar al lazo while
        CR1 = "true"
        CR2 = "true"
        CR3 = "true"
        CR4 = "true"
        CR5 = "true"

        # para inicial el while, el contador "k"(o numero de iteraciones) empieza en cero
        k = 0
        fgs = np.zeros(10)  # sirve para ver si se cumple el CR3

        while CR1 == "true" and CR2 == "true" and CR3 == "true" and CR4 == "true" and CR5 == "true":

            w = self.formula_w(1, p, g, k)

            # print "k  ******************-seriee*******************", k
            # print "g"
            # for j in range(self.model_parameters.dimension):
            #     print g[j],",",
            # print ""
            # print ""
            #
            # print "p"
            # for i in range(self.user_parameters.particles_number):
            #     for j in range(self.model_parameters.dimension):
            #         print p[i][j],",",
            #     print ""
            # print ""
            #
            # print "x"
            # for i in range(self.user_parameters.particles_number):
            #     for j in range(self.model_parameters.dimension):
            #         print x[i][j],",",
            #     print ""
            # print ""
            #
            # print "v"
            # for i in range(self.user_parameters.particles_number):
            #     for j in range(self.model_parameters.dimension):
            #         print v[i][j],",",
            #     print ""
            # print ""

            # Actualizacion de la velocidad
            v = w * v + self.user_parameters.c1 * initial_values.r1[k, :, :] * (p - x) + self.user_parameters.c2 * initial_values.r2[k, :, :] * (g - x)
            #v = (g - x)


            # print "v finall SERIE"
            # for i in range(self.user_parameters.particles_number):
            #     for j in range(self.model_parameters.dimension):
            #         print v[i][j],",",
            #     print ""
            # print ""


            # Se compara la velocidad con la maxima permitida,para prevenir que la particula no se extienda del limite
            np.clip(v, -initial_values.v_max_initial, initial_values.v_max_initial,
                    out=v)  # para la matriz v, se fija que cada elemento este dentro del v_max y vmin
            # correspondiente a cada particula


            # Actualizacion de la posicion
            x = (x + v).astype(np.float32)

            # np.clip(x, limit_low_V, limit_up_V, out=x) #-->LIMITES EN LA CAJA

            # Llamo a la function_serial que importe desde el modelo, y obtengo el valor de la function_serial objetivo (f) y el
            # TAV para cada particula

            f, TAV = self.model_parameters.function_serial(x, self.user_parameters.particles_number, self.model_parameters.limit_up_V, self.model_parameters.limit_low_V)

            k = k + 1

            if k == 1:
                # inicialmente u es la media de todos los TAV
                u = np.sum(TAV) / self.user_parameters.particles_number

            else:
                # ff=0 para hacer la sumatoria dentro del lazo a ver cuantas particulas resultan factibles (TAV<u), sirve
                # para luego actualizar u a medida que aumentan se recorre el lazo
                ff = 0
                for i in range(self.user_parameters.particles_number):
                    if TAV[i] <= u:
                        ff = ff + 1

                # recordar que FF o ff cuentan la cantidad de particulas factibles, que se usan para actualizar u
                FF = ff
                u = self.formula_u(1, u, FF, self.user_parameters.particles_number)  # actualiza u

            if np.any((TAV <= u) & (TAVP > u)):
                # si en la misma posicion que TAV<u resulta TAVP>u, entonces TAVP se actualiza con esa nueva posicion

                fp[(TAV <= u) & (TAVP > u)] = f[(TAV <= u) & (TAVP > u)]
                TAVP[(TAV <= u) & (TAVP > u)] = TAV[(TAV <= u) & (TAVP > u)]
                p[(TAV <= u) & (TAVP > u)] = x[(TAV <= u) & (TAVP > u)]

            # si TAVi es factible y TAVP tambien, se queda con la particula cuya function_serial sea mas chica.
            # Por eso si f[i] es menor que fg se queda con ella, y sino con la que habia encontrado anteriormente.
            if np.any((TAV <= u) & (TAVP <= u) & (f <= fp)):
                TAVP[(TAV <= u) & (TAVP <= u) & (f <= fp)] = TAV[(TAV <= u) & (TAVP <= u) & (f <= fp)]
                fp[(TAV <= u) & (TAVP <= u) & (f <= fp)] = f[(TAV <= u) & (TAVP <= u) & (f <= fp)]
                p[(TAV <= u) & (TAVP <= u) & (f <= fp)] = x[(TAV <= u) & (TAVP <= u) & (f <= fp)]

            # si tanto TAVi como TAVP son infactibles, se queda con aquella particula cuya violacion a las
            # restricciones sea menor. Por eso si TAVi es menor que TAVP me quedo con esa particula, y sino con la
            # que habia encontrado anteriormente.
            if np.any((TAV > u) & (TAVP > u) & (TAV <= TAVP)):
                fp[(TAV > u) & (TAVP > u) & (TAV <= TAVP)] = f[(TAV > u) & (TAVP > u) & (TAV <= TAVP)]
                TAVP[(TAV > u) & (TAVP > u) & (TAV <= TAVP)] = TAV[(TAV > u) & (TAVP > u) & (TAV <= TAVP)]
                p[(TAV > u) & (TAVP > u) & (TAV <= TAVP)] = x[(TAV > u) & (TAVP > u) & (TAV <= TAVP)]

            for i in range(self.user_parameters.particles_number):
                # else:
                if (TAV[i] <= u and TAVG > u):
                    fg = f[i]
                    TAVG = TAV[i]
                    g = x[i]

                if (TAV[i] <= u and TAVG <= u):
                    if (f[i] <= fg):
                        fg = f[i]
                        TAVG = TAV[i]
                        g = x[i]

                if (TAV[i] > u and TAVG > u):
                    if (TAV[i] <= TAVG):
                        fg = f[i]
                        TAVG = TAV[i]
                        g = x[i]

            # este if se usa para que quede guardado en que iteracion y en que tiempo u<0.001
            if u <= 0.001 and a == 0:
                a = k

            # CRITERIOS DE TERMINACION
            # CR1 = criterio de terminacion por exceder la maxima cantidad de iteraciones
            if k < self.user_parameters.iterations_max:
                CR1 = "true"
            else:
                CR1 = "false"
            # print "CR1 no se cumple"

            # CR2 = criterio de terminacion por exceder el tiempo maximo
            #    crittiempo= time.time()-tiempo_inicial
            #    if crittiempo<= time_max:
            #        CR2="true"
            #    else:
            #        CR2="false"
            #        print "CR2 no se cumple"

            # CR3 = criterio de terminacion por no disminuir considerablemente fg con las iteraciones

            # if k>=15:
            #     CRIT = abs(fg - fgh[k-15])
            #     if CRIT >= error:
            #         CR3 = "true"
            #     else:
            #         CR3 = "false"

            # CR4 = criterio de terminacion por alcanzar un valor de u muy bajo
            #    if u>=0.0000000001:
            #        CR4="true"
            #    else:
            #        CR4="false"
            #        print "CR4 no se cumple"

            # CR5 = criterio de terminacion por exceder el maximo numero de evaluacion de funciones (particles_number*iteraciones_maximas)
            #    evf= particles_number*k   #num de evaluacion de funciones
            #    if evf<=50000:
            #        CR5="true"
            #    else:
            #        CR5="false"
            #        print "CR5 no se cumple"

            # calculo del error entre el minimo valor de la function_serial obtenido por el pso(fg) y el verdadero (solution_global)
            if self.model_parameters.solution_global == 0:
                error = np.float32(abs(fg - self.model_parameters.solution_global))
            else:
                error = np.float32(abs(fg - self.model_parameters.solution_global) / abs(self.model_parameters.solution_global))

            # este if se usa para que quede guardado en que iteracion y en que tiempo error<0.01
            if error <= 0.01 and b == 0:
                b = k

            # print "fg", fg
            # print "u", u
            # print "g", g

        serial_results = Results(fg, error, TAVG, g, u)
        return serial_results
