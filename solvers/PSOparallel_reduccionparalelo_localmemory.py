
import numpy as np
# libreria que uso para poder paralelizar
import pycuda.driver as pycuda

from common.results import Results
from pycuda import compiler

BUSCAR = "//funcion objetivo"

kernel_modelo = """
    #include <stdio.h>
    #include <math.h>
    __global__ void kernel_pso_parallel(float *u_final, float *TAVP_final, float *TAVG_final, float *error_final, float *g_final,float *fg_final, float *fp_final, float *p_final, float *v_final, float *x_final, float *v, float *r1, float *x, float *r2, float *g, float *limit_up_V, float *limit_low_V, float *fg, float *vmax, float *fgreal, float *TAV, float TAVmin, float *TAVG, float *u, float *TAVP, float *f)
    {
        const int i = blockDim.x * blockIdx.x + threadIdx.x;
        const int j = blockDim.y * blockIdx.y + threadIdx.y;
        
        
        
                //CONSTANTES
        const int particles_number=%(num_part)s;
        const int dimension=%(dimension)s;
        const float c1=%(c1)s;
        const float c2=%(c2)s;
        const float w=%(w)s;
        const int iteraciones_maximas=%(iteraciones_maximas)s;

        //MEMORIA COMPARTIDA
        __shared__ float v_s[particles_number*dimension];
        __shared__ float x_s[particles_number*dimension];
        __shared__ float v_final_s[particles_number*dimension];
        __shared__ float x_final_s[particles_number*dimension];
        __shared__ float vmax_s[dimension];
        __shared__ float fgreal_s[1];
        __shared__ float u_s[1];
        __shared__ float u_final_s[1];
        __shared__ float f_s[particles_number];
        __shared__ float fp_final_s[particles_number];
        __shared__ float p_final_s[particles_number*dimension];
        __shared__ float TAVP_final_s[particles_number];
        __shared__ float error_s[1];
        __shared__ float g_s[dimension];
        __shared__ float TAVG_s[1];
        __shared__ float fg_s[1];
        __shared__ float TAV_s[particles_number];

        //ASIGNACION DE MEMORIA GLOBAL A MEMORIA COMPARTIDA
        v_s[ i*dimension + j]=v[ i*dimension + j];
        x_s[ i*dimension + j]=x[ i*dimension + j];
        v_final_s[ i*dimension + j]=0;
        x_final_s[ i*dimension + j]=0;
        vmax_s[j]=vmax[j];
        fgreal_s[0]=fgreal[0];
        u_s[0]=u[0];
        f_s[i]=f[i];
        fp_final_s[i] = f[i];
        p_final_s[i*dimension+j] = x[i*dimension+j];
        TAVP_final_s[i] = TAV_s[i];
        fg_s[0] =  fg[0];
        TAVG_s[0]= TAVG[0];
        g_s[j]= g[j];



        int CR1=1;
        int k=0;

        fp_final[i] = f[i];
        p_final[i*dimension+j] = x[i*dimension+j];
        TAVP_final[i] = TAVP[i];

        // uso estas variables para la reduccion del mejor global en paralelo
        __shared__ float g_final2[dimension];
        __shared__ float TAVG_final2[1];
        __shared__ float fg_final2[1];

        if (i==0){

            fg_final2[i] =  fg[i];
            TAVG_final2[i]= TAVG[i];
            g_final2[j]= g[j];
        }

        __syncthreads();


        while(CR1==1){

            v_final[ i*dimension + j ] =  w * v[ i*dimension + j ] + c1 * r1[ i*dimension + j + k*dimension*particles_number] * ( p_final[ i*dimension + j ] - x[ i*dimension + j ] ) + c2 * r2[ i*dimension + j + k*dimension*particles_number] * (g_final2[ j ] - x[ i*dimension + j ]);

            __syncthreads();



            if (v_final[ i*dimension + j ]>abs(vmax[j])){
                v_final[ i*dimension + j ] = abs(vmax[j]);
            }

            if (v_final[ i*dimension + j ]<-abs(vmax[j])){
                v_final[ i*dimension + j ] = -abs(vmax[j]);
            }

            v[ i*dimension + j ] = v_final[ i*dimension + j ];

            __syncthreads();


            x_final[ i*dimension + j ] = x[ i*dimension + j ] + v_final[ i*dimension + j ] ;

            __syncthreads();

            x[i*dimension+j]=x_final[i*dimension+j];
            __syncthreads();


            ///////////////////calculo de la funcion de cada modelo en particular /////////////////////////////////

            //funcion objetivo

            ////////////////////////////////////////////////////////////////////////////////////////////////////

            k=k+1;
            __syncthreads();

            //-----------------busqueda del mejor local y global----------------------------------------------------------------------------------------
            if (i == 0 and j==0){
                if (k==1){
                    float sumaTAV=0;
                    for (int aa=0; aa < particles_number; aa++){
                        sumaTAV=sumaTAV+TAV[aa];
                    }
                    u[i]=sumaTAV/particles_number;
                }

                else{
                    int ff=0;  //sirve para la actualizacion de u
                    for (int aa=0; aa < particles_number; aa++){
                        if (TAV[aa]<=u[i]){
                            ff=ff+1;
                        }
                    }
                    int FF=ff;

                    u_final[i]=u[i]*(1-(FF*1.0/particles_number));

                    u[i]=u_final[i];
                }
            }
            __syncthreads();

            if ((TAV[i]<=u[0]) & (TAVP_final[i]>u[0])){
                fp_final[i] = f[i];
                TAVP_final[i] = TAV[i];
                p_final[i*dimension+j] = x[i*dimension+j];
            }
            if ((TAV[i]<=u[0]) & (TAVP_final[i]<=u[0]) & (f[i]<=fp_final[i])){
                fp_final[i] = f[i];
                TAVP_final[i] = TAV[i];
                p_final[i*dimension+j] = x[i*dimension+j];
            }


            if ((TAV[i]>u[0]) & (TAVP_final[i]>u[0]) & (TAV[i]<=TAVP_final[i])){
                fp_final[i] = f[i];
                TAVP_final[i] = TAV[i];
                p_final[i*dimension+j] = x[i*dimension+j];
            }

            __syncthreads();



              ///////////////////////////////////  MEJOR GLOBAL REDUCCION PARALELO   ///////////////////////////////////////////////////

            //creo los vectores candidatos, que son los valores actuales comparados con los mejores obtenidos previamente
            __shared__ float TAVGcandidato[particles_number];
            __shared__ float fgcandidato[particles_number];
            __shared__ float gcandidato[particles_number*dimension+dimension];

            TAVGcandidato[i]=TAVG_final2[0];
            fgcandidato[i]=fg_final2[0];
            gcandidato[i*dimension+j]=g_final2[j];

            __syncthreads();
            if ((TAV[i]<=u[0]) & (TAVGcandidato[i]>u[0])){
                fgcandidato[i] = f[i];
                TAVGcandidato[i] = TAV[i];
                gcandidato[i*dimension+j] = x[i*dimension+j];
            }
            if ((TAV[i]<=u[0]) & (TAVGcandidato[i]<=u[0]) & (f[i]<=fgcandidato[i])){
                fgcandidato[i] = f[i];
                TAVGcandidato[i] = TAV[i];
                gcandidato[i*dimension+j] = x[i*dimension+j];
            }

            if ((TAV[i]>u[0]) & (TAVGcandidato[i]>u[0]) & (TAV[i]<=TAVGcandidato[i])){
                fgcandidato[i] = f[i];
                TAVGcandidato[i] = TAV[i];
                gcandidato[i*dimension+j] = x[i*dimension+j];
            }

            __syncthreads();

            //calculo TAVcandidato menos u, para saber si existen particulas factibles o no, y asi realizar el
            //tratamiento correspondiente

            __shared__ float TAVGcandidatomenosu[particles_number];
            TAVGcandidatomenosu[i]=TAVGcandidato[i]-u[0];

            //////////////// REDUCCION ARIEL PARA ENCONTRAR EL MINIMO DE TAVCANDIDATOMENOSU ///////////////////////////
            __shared__ float sdata[particles_number];
            __shared__ int sindice[particles_number];

            int tid = threadIdx.x;


            sdata[tid] = TAVGcandidatomenosu[i];
            sindice[tid] = tid;


            __syncthreads();

            int s = blockDim.x / 2;

            while(s>1){

                __syncthreads();
                if (tid < s ) {
                    if (sdata[tid] >= sdata[tid + s]) {

                        sdata[tid] = sdata[tid + s];
                        sindice[tid] = sindice[tid + s];

                    }
                    __syncthreads();
                }

                if(s%%2==0)
                    s=s/2;
                else
                    s=(s+1)/2;

                __syncthreads();

                if(s<=0)break;

            }
            __syncthreads();


            if (tid == 0) {
                if(sdata[0]>sdata[1]){

                    sdata[0]=sdata[1];
                    sindice[0]=sindice[1];

                }
                if(sdata[0]>sdata[particles_number-1]){

                    sdata[0]=sdata[particles_number-1];
                    sindice[0]=sindice[particles_number-1];

                }
            }

            __shared__ float TAVGcandidatominimo;
            __shared__ int indiceminimoTAVG;

            if (tid == 0) {
                TAVGcandidatominimo = sdata[0];
                indiceminimoTAVG = sindice[0];
            }

            // si TAVGcandidatominimo es mayor que cero, se trata de una solucion infactible, por lo tanto me quedo con
            // la de menor TAV, que es TAVGcandidatominimo

            if (TAVGcandidatominimo>=0){
                if(i==0){
                g_final2[j]=gcandidato[indiceminimoTAVG*dimension+j];
                TAVG_final2[i]=TAVGcandidato[indiceminimoTAVG];
                fg_final2[i]=fgcandidato[indiceminimoTAVG];
                }
            }

            __syncthreads();

            if (TAVGcandidatominimo<0){
                if(TAVGcandidatomenosu[i]>=0){
                    fgcandidato[i]=969696969696969696;
                }

                __syncthreads();

                ///////NUEVAMENTE REDUCCION ARIEL,PERO AHORA PARA ENCONTRAR EL MINIMO DE LA FUNCION OBJETIVO//////////////
                sdata[tid] = fgcandidato[i];

                sindice[tid] = tid;

                __syncthreads();

                int s = blockDim.x / 2;

                while(s>1){
                    __syncthreads();
                    if (tid < s ) {
                        if (sdata[tid] >= sdata[tid + s]) {

                            sdata[tid] = sdata[tid + s];
                            sindice[tid] = sindice[tid + s];

                        }
                        __syncthreads();
                    }

                    if(s%%2==0)
                        s=s/2;
                    else
                        s=(s+1)/2;

                    __syncthreads();

                    if(s<=0)break;

                }
                __syncthreads();



                if (tid == 0) {
                    if(sdata[0]>sdata[1]){

                        sdata[0]=sdata[1];
                        sindice[0]=sindice[1];

                    }
                    if(sdata[0]>sdata[particles_number-1]){

                        sdata[0]=sdata[particles_number-1];
                        sindice[0]=sindice[particles_number-1];

                    }
                }

                __shared__ float fgcandidatominimo;
                __shared__ int indiceminimofg;

                if (tid == 0) {
                    fgcandidatominimo = sdata[0];
                    indiceminimofg = sindice[0];

                }

                __syncthreads();

                //#if __CUDA_ARCH__ >=200
                //if (j==0 and i==0){
                //    printf("fgcandidato   ");
                //    for(int iii=0; iii<particles_number; iii++){
                //        printf("%%f, ", fgcandidato[iii]);
                //    }
                //    printf("\\n");
                //    printf("fgcandidatominimo   ");
                //    printf("%%f",fgcandidatominimo);
                //    printf("\\n");
                //}
                //#endif

                if (i==0){
                    g_final2[j]=gcandidato[indiceminimofg*dimension+j];
                    TAVG_final2[i]=TAVGcandidato[indiceminimofg];
                    fg_final2[i]=fgcandidato[indiceminimofg];
                }
            }

            if (k>=iteraciones_maximas){
                CR1=0;
            }

            if (fgreal[0]==0){
                error_final[i]=abs(fgreal[0]-fg_final2[0]);
            }
            if (fgreal[0]!=0){
                error_final[i]=abs(fgreal[0]-fg_final2[0])/abs(fgreal[0]);
            }


            //#if __CUDA_ARCH__ >=200
            //if (j==0 and i==0){
            //    printf("u %%f\\n", u[i]);
            //    printf("g   ");
            //    for (int jjj=0; jjj<dimension; jjj++){
            //        printf("%%f, ", g[jjj]);
            //    }
            //    printf("\\n \\n");
            //}
            //#endif


        }//FIN lazo while

        __syncthreads();


        if (i==0){
            fg_final[i] =  fg_final2[i];
            TAVG_final[i]=TAVG_final2[i];
            g_final[j]= g_final2[j];
        }

     }//FIN kernel

"""


class PSOParallel:
    def __init__(self, user_parameters, model_parameters):
        kernel_code = kernel_modelo.replace(BUSCAR, model_parameters.function_parallel)
        kernel_code = kernel_code % {'num_part': user_parameters.particles_number,
                                     'dimension': model_parameters.dimension,
                                     'c1': user_parameters.c1, 'c2': user_parameters.c2, 'w': user_parameters.w,
                                     'iteraciones_maximas': user_parameters.iterations_max}

        kernel_code = compiler.SourceModule(kernel_code)
        self.kernel_compilado = kernel_code.get_function("kernel_pso_parallel")
        self.user_parameters = user_parameters
        self.model_parameters = model_parameters

    def compute(self, initial_values):
        v = (initial_values.v_initial).astype(np.float32)
        v_final = (v).astype(np.float32)
        r1 = (initial_values.r1).astype(np.float32)
        x = (initial_values.x_initial).astype(np.float32)
        x_final = (x).astype(np.float32)
        r2 = (initial_values.r2).astype(np.float32)
        g = (initial_values.g_initial).astype(np.float32)

        self.model_parameters.limit_up_V = (self.model_parameters.limit_up_V).astype(np.float32)
        self.model_parameters.limit_low_V = (self.model_parameters.limit_low_V).astype(np.float32)
        f = (initial_values.f_initial).astype(np.float32)
        TAV = (initial_values.TAV_initial).astype(np.float32)
        fp_final = np.zeros_like(f).astype(np.float32)
        p_final = np.zeros_like(x).astype(np.float32)
        fg = np.array(initial_values.fg_initial).astype(np.float32)
        fg_final = np.array(0).astype(np.float32)
        g_final = (g).astype(np.float32)
        v_maxinicial = (initial_values.v_max_initial).astype(np.float32)
        solution_global_V = np.array(self.model_parameters.solution_global).astype(np.float32)
        error_final = np.array(0).astype(np.float32)
        u = np.array(initial_values.u_initial).astype(np.float32)

        TAVP_final = np.zeros_like(initial_values.TAVP_initial).astype(np.float32)
        f = (initial_values.f_initial).astype(np.float32)
        TAVP = (initial_values.TAVP_initial).astype(np.float32)

        TAVmin = np.float32(999999999999)
        TAVG = np.array(initial_values.TAVG_initial).astype(np.float32)
        TAVG_final = np.array(99999).astype(np.float32)
        u_final = np.zeros_like(u).astype(np.float32)

        self.kernel_compilado(pycuda.Out(u_final), pycuda.Out(TAVP_final), pycuda.Out(TAVG_final),
                            pycuda.Out(error_final),
                            pycuda.Out(g_final), pycuda.Out(fg_final), pycuda.Out(fp_final),
                            pycuda.Out(p_final), pycuda.Out(v_final),
                            pycuda.Out(x_final), pycuda.In(v),
                            pycuda.In(r1), pycuda.In(x),
                            pycuda.In(r2), pycuda.In(g), pycuda.In(self.model_parameters.limit_up_V),
                            pycuda.In(self.model_parameters.limit_low_V), pycuda.In(fg), pycuda.In(v_maxinicial),
                            pycuda.In(solution_global_V), pycuda.In(TAV), np.float32(TAVmin), pycuda.In(TAVG),
                            pycuda.In(u),
                            pycuda.In(TAVP), pycuda.In(f),
                            block=(self.user_parameters.particles_number,self.model_parameters.dimension, 1),
                            grid=(1, 1, 1),
                            )

        parallel_results = Results(fg_final, error_final, TAVG_final, g_final, u_final)
        return parallel_results
