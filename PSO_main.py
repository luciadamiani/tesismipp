import time

from common.initial_values import InitialValues
from common.input import *
from common.keeper import Keeper
from common.model_parameters import ModelParameters
#from solvers.PSOparallel import PSOParallel
from solvers.PSOparallel_reduccionparalelo_localmemory import PSOParallel
from solvers.PSOserial import PSOSerial

def run_pso_serial(user_parameters, model_parameters, solver_serial, keeper_serial):
    # Loop evaluation, created to evaluate "user_parameters.evaluations_number" times the same model function
    for evaluation in range(user_parameters.evaluations_number):
        initial_time = time.time()

        # Calculate the initial values
        initial_values = InitialValues(model_parameters, user_parameters)

        serial_results = solver_serial.compute(initial_values)

        final_time_serial = time.time()
        total_time_serial = final_time_serial - initial_time

        keeper_serial.save_run(serial_results, total_time_serial)

        if evaluation == user_parameters.evaluations_number - 1:
            keeper_serial.write_to_excel(user_parameters, model_parameters, user_parameters.out_file_name_serial)

def run_pso_parallel(user_parameters, model_parameters, solver_parallel, keeper_parallel):
    # Loop evaluation, created to evaluate "user_parameters.evaluations_number" times the same model function
    for evaluation in range(user_parameters.evaluations_number):
        initial_time = time.time()

        # Calculate the initial values
        initial_values = InitialValues(model_parameters, user_parameters)

        parallel_results = solver_parallel.compute(initial_values)

        final_time_parallel = time.time()
        total_time_parallel = final_time_parallel - initial_time

        keeper_parallel.save_run(parallel_results, total_time_parallel)

        if evaluation == user_parameters.evaluations_number - 1:
            keeper_parallel.write_to_excel(user_parameters, model_parameters,
                                           user_parameters.out_file_name_parallel)


def run_pso_parallel_and_serial(user_parameters, model_parameters, solver_parallel, keeper_parallel, solver_serial, keeper_serial):
    # Loop evaluation, created to evaluate "user_parameters.evaluations_number" times the same model function
    for evaluation in range(user_parameters.evaluations_number):
        initial_time = time.time()

        # Calculate the initial values
        initial_values = InitialValues(model_parameters, user_parameters)

        intermediate_time_parallel = time.time()

        parallel_results = solver_parallel.compute(initial_values)

        final_time_parallel = time.time()

        total_time_parallel = final_time_parallel - initial_time

        keeper_parallel.save_run(parallel_results, total_time_parallel)

        pso_time_parallel = final_time_parallel - intermediate_time_parallel

        serial_results = solver_serial.compute(initial_values)
        final_time_serial = time.time()
        total_time_serial = final_time_serial - pso_time_parallel - initial_time

        keeper_serial.save_run(serial_results, total_time_serial)

        # Save serial and parallel results in excel
        if evaluation == user_parameters.evaluations_number - 1:
            keeper_parallel.write_to_excel(user_parameters, model_parameters, user_parameters.out_file_name_parallel)
            keeper_serial.write_to_excel(user_parameters, model_parameters, user_parameters.out_file_name_serial)


def run_pso():
    # Read user parameters
    user_parameters = read_input()
    print(user_parameters)

    # Read model parameters
    model_parameters = ModelParameters(user_parameters.model_id)

    # Keep serial and load solver
    keeper_serial = Keeper()
    solver_serial = PSOSerial(user_parameters, model_parameters)

    # Keep parallel and load solver
    if user_parameters.process_type == PROCESS_TYPE_PARALLEL or user_parameters.process_type == PROCESS_TYPE_PARALLEL_AND_SERIAL:
        keeper_parallel = Keeper()
        solver_parallel = PSOParallel(user_parameters, model_parameters)

    # Run PSO serial
    if user_parameters.process_type == PROCESS_TYPE_SERIAL:
        run_pso_serial(user_parameters, model_parameters, solver_serial, keeper_serial)

    # Run PSO parallel
    if user_parameters.process_type == PROCESS_TYPE_PARALLEL:
        run_pso_parallel(user_parameters, model_parameters, solver_parallel, keeper_parallel)

    # Run PSO serial and parallel
    if user_parameters.process_type == PROCESS_TYPE_PARALLEL_AND_SERIAL:
        run_pso_parallel_and_serial(user_parameters, model_parameters, solver_parallel, keeper_parallel, solver_serial,keeper_serial)

if __name__ == '__main__':
    run_pso()
