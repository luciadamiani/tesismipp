from common.user_parameters import UserParameters

# Constants
PROCESS_TYPE_PARALLEL_AND_SERIAL = 2
PROCESS_TYPE_PARALLEL = 1
PROCESS_TYPE_SERIAL = 0


# Function that reads the parameters that the user enters by keyboard
def read_input():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='%(prog)s is an ArgumentParser demo')
    # Argumento posicional. Los argumentos posicionales son obligatorios.
    # para que sea opcional va con el '-...', si no se pone nada es opcional
    # para ingresar un parametro opcional--> python ARCHIVO.py 1(del modelo, obligatorio)-N 20 por ejemplo
    parser.add_argument('numero_modelo', type=int)
    parser.add_argument('-particles_number', type=int, default=50)
    parser.add_argument('modo_calculo', type=int)
    parser.add_argument('-iterations_max', type=int, default=2500)
    parser.add_argument('-c1', type=float, default=2)
    parser.add_argument('-c2', type=float, default=2)
    parser.add_argument('-w', type=float, default=0.7)
    parser.add_argument('-evaluations_number', type=int, default=1)
    parser.add_argument('nombre_archivo_paralelo')
    parser.add_argument('nombre_archivo_serie')
    args = parser.parse_args()
    model_id = args.numero_modelo
    particles_number = args.particles_number
    process_type = args.modo_calculo
    iterations_max = args.iterations_max
    c1 = args.c1
    c2 = args.c2
    w = args.w
    evaluations_number = args.evaluations_number
    out_file_name_parallel = args.nombre_archivo_paralelo
    out_file_name_serial = args.nombre_archivo_serie
    parameters = UserParameters(model_id, particles_number, process_type, iterations_max, c1, c2, w, evaluations_number,
                                out_file_name_parallel, out_file_name_serial)
    return parameters
