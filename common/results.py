# Class that contain fg(global solution), error(relative error between fg and real solution), TAVG(global violated restrictions), g(global position)
class Results:
    def __init__(self, fg, error, TAVG, g, u):
        self.fg = fg
        self.error = error
        self.TAVG = TAVG
        self.g = g
        self.u = u