# Class that contain
class UserParameters:
    def __init__(self, model_id, particles_number, process_type, iterations_max, c1, c2, w, evaluations_number,
                 out_file_name_parallel, out_file_name_serial):
        self.model_id = model_id
        self.particles_number = particles_number
        self.process_type = process_type
        self.iterations_max = iterations_max
        self.c1 = c1
        self.c2 = c2
        self.w = w
        self.evaluations_number = evaluations_number
        self.out_file_name_parallel = out_file_name_parallel
        self.out_file_name_serial = out_file_name_serial
        self.time_max = -1

    def __str__(self):
        return "Parameters: \n model_id % d \n particles_number % d \n process_type % d \n " \
               "iterations_max % d \n c1 % f \n c2 % f \n w % f \n evaluations_number % d \n " \
               "out_file_name_parallel % s \n out_file_name_serial % s \n time_max % f \n " % (
                   self.model_id, self.particles_number, self.process_type,
                   self.iterations_max, self.c1, self.c2, self.w, self.evaluations_number,
                   self.out_file_name_parallel, self.out_file_name_serial, self.time_max)



