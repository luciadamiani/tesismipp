import numpy as np
import math
from pycuda.compiler import SourceModule
import pycuda.autoinit
import pycuda.driver as pycuda

import models

# Class that load the parameters of each model
class ModelParameters:
    def __init__(self, model_id):
        if model_id == 1: self.load_parameters_model_1()
        if model_id == 2: self.load_parameters_model_2()
        if model_id == 3: self.load_parameters_model_3()
        if model_id == 4: self.load_parameters_model_4()
        if model_id == 5: self.load_parameters_model_5()
        if model_id == 6: self.load_parameters_model_6()
        if model_id == 7: self.load_parameters_model_7()
        if model_id == 8: self.load_parameters_model_8()
        if model_id == 9: self.load_parameters_model_9()
        if model_id == 10: self.load_parameters_model_10()
        if model_id == 11: self.load_parameters_model_11()
        if model_id == 13: self.load_parameters_model_13()
        if model_id == 14: self.load_parameters_model_14()
        if model_id == 15: self.load_parameters_model_15()
        if model_id == 18: self.load_parameters_model_18()
        if model_id == 24: self.load_parameters_model_24()
        if model_id == 101: self.load_parameters_model_101()
        if model_id == 102: self.load_parameters_model_102()
        if model_id == 103: self.load_parameters_model_103()
        if model_id == 104: self.load_parameters_model_104()
        if model_id == 105: self.load_parameters_model_105()
        if model_id == 106: self.load_parameters_model_106()
        if model_id == 107: self.load_parameters_model_107()
        if model_id == 108: self.load_parameters_model_108()
        if model_id == 109: self.load_parameters_model_109()
        if model_id == 110: self.load_parameters_model_110()
        if model_id == 111: self.load_parameters_model_111()
        if model_id == 112: self.load_parameters_model_112()


    def load_parameters_model_1(self):
        from models import modelo_01
        self.dimension = models.modelo_01.dimension
        self.limit_up_V = models.modelo_01.limit_up_V
        self.limit_low_V = models.modelo_01.limit_low_V
        self.function_serial = models.modelo_01.function_serial
        self.function_parallel = models.modelo_01.function_parallel
        self.solution_global = models.modelo_01.solution_global
        self.solution_position_V = models.modelo_01.solution_position_V
        self.time_max = models.modelo_01.time_max
        self.error = models.modelo_01.error

    def load_parameters_model_2(self):
        from models import modelo_02
        self.dimension = models.modelo_02.dimension
        self.limit_up_V = models.modelo_02.limit_up_V
        self.limit_low_V = models.modelo_02.limit_low_V
        self.function_serial = models.modelo_02.function_serial
        self.function_parallel = models.modelo_02.function_parallel
        self.solution_global = models.modelo_02.solution_global
        self.solution_position_V = models.modelo_02.solution_position_V
        self.time_max = models.modelo_02.time_max

    def load_parameters_model_3(self):
        from models import modelo_03
        self.dimension = models.modelo_03.dimension
        self.limit_up_V = models.modelo_03.limit_up_V
        self.limit_low_V = models.modelo_03.limit_low_V
        self.function_serial = models.modelo_03.function_serial
        self.function_parallel = models.modelo_03.function_parallel
        self.solution_global = models.modelo_03.solution_global
        self.solution_position_V = models.modelo_03.solution_position_V
        self.time_max = models.modelo_03.time_max
        self.error = models.modelo_03.error

    def load_parameters_model_4(self):
        from models import modelo_04
        self.dimension = models.modelo_04.dimension
        self.limit_up_V = models.modelo_04.limit_up_V
        self.limit_low_V = models.modelo_04.limit_low_V
        self.function_serial = models.modelo_04.function_serial
        self.function_parallel = models.modelo_04.function_parallel
        self.solution_global = models.modelo_04.solution_global
        self.solution_position_V = models.modelo_04.solution_position_V
        self.time_max = models.modelo_04.time_max
        self.error = models.modelo_04.error

    def load_parameters_model_5(self):
        from models import modelo_05
        self.dimension = models.modelo_05.dimension
        self.limit_up_V = models.modelo_05.limit_up_V
        self.limit_low_V = models.modelo_05.limit_low_V
        self.function_serial = models.modelo_05.function_serial
        self.function_parallel = models.modelo_05.function_parallel
        self.solution_global = models.modelo_05.solution_global
        self.solution_position_V = models.modelo_05.solution_position_V
        self.time_max = models.modelo_05.time_max
        self.error = models.modelo_05.error

    def load_parameters_model_6(self):
        from models import modelo_06
        self.dimension = models.modelo_06.dimension
        self.limit_up_V = models.modelo_06.limit_up_V
        self.limit_low_V = models.modelo_06.limit_low_V
        self.function_serial = models.modelo_06.function_serial
        self.function_parallel = models.modelo_06.function_parallel
        self.solution_global = models.modelo_06.solution_global
        self.solution_position_V = models.modelo_06.solution_position_V
        self.time_max = models.modelo_06.time_max
        self.error = models.modelo_06.error

    def load_parameters_model_7(self):
        from models import modelo_07
        self.dimension = models.modelo_07.dimension
        self.limit_up_V = models.modelo_07.limit_up_V
        self.limit_low_V = models.modelo_07.limit_low_V
        self.function_serial = models.modelo_07.function_serial
        self.function_parallel = models.modelo_07.function_parallel
        self.solution_global = models.modelo_07.solution_global
        self.solution_position_V = models.modelo_07.solution_position_V
        self.time_max = models.modelo_07.time_max
        self.error = models.modelo_07.error

    def load_parameters_model_8(self):
        from models import modelo_08
        self.dimension = models.modelo_08.dimension
        self.limit_up_V = models.modelo_08.limit_up_V
        self.limit_low_V = models.modelo_08.limit_low_V
        self.function_serial = models.modelo_08.function_serial
        self.function_parallel = models.modelo_08.function_parallel
        self.solution_global = models.modelo_08.solution_global
        self.solution_position_V = models.modelo_08.solution_position_V
        self.time_max = models.modelo_08.time_max
        self.error = models.modelo_08.error

    def load_parameters_model_9(self):
        from models import modelo_09
        self.dimension = models.modelo_09.dimension
        self.limit_up_V = models.modelo_09.limit_up_V
        self.limit_low_V = models.modelo_09.limit_low_V
        self.function_serial = models.modelo_09.function_serial
        self.function_parallel = models.modelo_09.function_parallel
        self.solution_global = models.modelo_09.solution_global
        self.solution_position_V = models.modelo_09.solution_position_V
        self.time_max = models.modelo_09.time_max
        self.error = models.modelo_09.error

    def load_parameters_model_10(self):
        from models import modelo_10
        self.dimension = models.modelo_10.dimension
        self.limit_up_V = models.modelo_10.limit_up_V
        self.limit_low_V = models.modelo_10.limit_low_V
        self.function_serial = models.modelo_10.function_serial
        self.function_parallel = models.modelo_10.function_parallel
        self.solution_global = models.modelo_10.solution_global
        self.solution_position_V = models.modelo_10.solution_position_V
        self.time_max = models.modelo_10.time_max
        self.error = models.modelo_10.error

    def load_parameters_model_11(self):
        from models import modelo_11
        self.dimension = models.modelo_11.dimension
        self.limit_up_V = models.modelo_11.limit_up_V
        self.limit_low_V = models.modelo_11.limit_low_V
        self.function_serial = models.modelo_11.function_serial
        self.function_parallel = models.modelo_11.function_parallel
        self.solution_global = models.modelo_11.solution_global
        self.solution_position_V = models.modelo_11.solution_position_V
        self.time_max = models.modelo_11.time_max
        self.error = models.modelo_11.error

    def load_parameters_model_13(self):
        from models import modelo_13
        self.dimension = models.modelo_13.dimension
        self.limit_up_V = models.modelo_13.limit_up_V
        self.limit_low_V = models.modelo_13.limit_low_V
        self.function_serial = models.modelo_13.function_serial
        self.function_parallel = models.modelo_13.function_parallel
        self.solution_global = models.modelo_13.solution_global
        self.solution_position_V = models.modelo_13.solution_position_V
        self.time_max = models.modelo_13.time_max
        self.error = models.modelo_13.error

    def load_parameters_model_14(self):
        from models import modelo_14
        self.dimension = models.modelo_14.dimension
        self.limit_up_V = models.modelo_14.limit_up_V
        self.limit_low_V = models.modelo_14.limit_low_V
        self.function_serial = models.modelo_14.function_serial
        self.function_parallel = models.modelo_14.function_parallel
        self.solution_global = models.modelo_14.solution_global
        self.solution_position_V = models.modelo_14.solution_position_V
        self.time_max = models.modelo_14.time_max
        self.error = models.modelo_14.error

    def load_parameters_model_15(self):
        from models import modelo_15
        self.dimension = models.modelo_15.dimension
        self.limit_up_V = models.modelo_15.limit_up_V
        self.limit_low_V = models.modelo_15.limit_low_V
        self.function_serial = models.modelo_15.function_serial
        self.function_parallel = models.modelo_15.function_parallel
        self.solution_global = models.modelo_15.solution_global
        self.solution_position_V = models.modelo_15.solution_position_V
        self.time_max = models.modelo_15.time_max
        self.error = models.modelo_15.error

    def load_parameters_model_18(self):
        from models import modelo_18
        self.dimension = models.modelo_18.dimension
        self.limit_up_V = models.modelo_18.limit_up_V
        self.limit_low_V = models.modelo_18.limit_low_V
        self.function_serial = models.modelo_18.function_serial
        self.function_parallel = models.modelo_18.function_parallel
        self.solution_global = models.modelo_18.solution_global
        self.solution_position_V = models.modelo_18.solution_position_V
        self.time_max = models.modelo_18.time_max
        self.error = models.modelo_18.error

    def load_parameters_model_24(self):
        from models import modelo_24
        self.dimension = models.modelo_24.dimension
        self.limit_up_V = models.modelo_24.limit_up_V
        self.limit_low_V = models.modelo_24.limit_low_V
        self.function_serial = models.modelo_24.function_serial
        self.function_parallel = models.modelo_24.function_parallel
        self.solution_global = models.modelo_24.solution_global
        self.solution_position_V = models.modelo_24.solution_position_V
        self.time_max = models.modelo_24.time_max
        self.error = models.modelo_24.error

    def load_parameters_model_101(self):
        from models import modelo_101
        self.dimension = models.modelo_101.dimension
        self.limit_up_V = models.modelo_101.limit_up_V
        self.limit_low_V = models.modelo_101.limit_low_V
        self.function_serial = models.modelo_101.function_serial
        self.function_parallel = models.modelo_101.function_parallel
        self.solution_global = models.modelo_101.solution_global
        self.solution_position_V = models.modelo_101.solution_position_V
        self.time_max = models.modelo_101.time_max
        self.error = models.modelo_101.error

    def load_parameters_model_102(self):
        from models import modelo_102
        self.dimension = models.modelo_102.dimension
        self.limit_up_V = models.modelo_102.limit_up_V
        self.limit_low_V = models.modelo_102.limit_low_V
        self.function_serial = models.modelo_102.function_serial
        self.function_parallel = models.modelo_102.function_parallel
        self.solution_global = models.modelo_102.solution_global
        self.solution_position_V = models.modelo_102.solution_position_V
        self.time_max = models.modelo_102.time_max
        self.error = models.modelo_102.error

    def load_parameters_model_103(self):
        from models import modelo_103
        self.dimension = models.modelo_103.dimension
        self.limit_up_V = models.modelo_103.limit_up_V
        self.limit_low_V = models.modelo_103.limit_low_V
        self.function_serial = models.modelo_103.function_serial
        self.function_parallel = models.modelo_103.function_parallel
        self.solution_global = models.modelo_103.solution_global
        self.solution_position_V = models.modelo_103.solution_position_V
        self.time_max = models.modelo_103.time_max
        self.error = models.modelo_103.error

    def load_parameters_model_104(self):
        from models import modelo_104
        self.dimension = models.modelo_104.dimension
        self.limit_up_V = models.modelo_104.limit_up_V
        self.limit_low_V = models.modelo_104.limit_low_V
        self.function_serial = models.modelo_104.function_serial
        self.function_parallel = models.modelo_104.function_parallel
        self.solution_global = models.modelo_104.solution_global
        self.solution_position_V = models.modelo_104.solution_position_V
        self.time_max = models.modelo_104.time_max
        self.error = models.modelo_104.error

    def load_parameters_model_105(self):
        from models import modelo_105
        self.dimension = models.modelo_105.dimension
        self.limit_up_V = models.modelo_105.limit_up_V
        self.limit_low_V = models.modelo_105.limit_low_V
        self.function_serial = models.modelo_105.function_serial
        self.function_parallel = models.modelo_105.function_parallel
        self.solution_global = models.modelo_105.solution_global
        self.solution_position_V = models.modelo_105.solution_position_V
        self.time_max = models.modelo_105.time_max
        self.error = models.modelo_105.error

    def load_parameters_model_106(self):
        from models import modelo_106
        self.dimension = models.modelo_106.dimension
        self.limit_up_V = models.modelo_106.limit_up_V
        self.limit_low_V = models.modelo_106.limit_low_V
        self.function_serial = models.modelo_106.function_serial
        self.function_parallel = models.modelo_106.function_parallel
        self.solution_global = models.modelo_106.solution_global
        self.solution_position_V = models.modelo_106.solution_position_V
        self.time_max = models.modelo_106.time_max
        self.error = models.modelo_106.error

    def load_parameters_model_107(self):
        from models import modelo_107
        self.dimension = models.modelo_107.dimension
        self.limit_up_V = models.modelo_107.limit_up_V
        self.limit_low_V = models.modelo_107.limit_low_V
        self.function_serial = models.modelo_107.function_serial
        self.function_parallel = models.modelo_107.function_parallel
        self.solution_global = models.modelo_107.solution_global
        self.solution_position_V = models.modelo_107.solution_position_V
        self.time_max = models.modelo_107.time_max
        self.error = models.modelo_107.error

    def load_parameters_model_108(self):
        from models import modelo_108
        self.dimension = models.modelo_108.dimension
        self.limit_up_V = models.modelo_108.limit_up_V
        self.limit_low_V = models.modelo_108.limit_low_V
        self.function_serial = models.modelo_108.function_serial
        self.function_parallel = models.modelo_108.function_parallel
        self.solution_global = models.modelo_108.solution_global
        self.solution_position_V = models.modelo_108.solution_position_V
        self.time_max = models.modelo_108.time_max
        self.error = models.modelo_108.error

    def load_parameters_model_109(self):
        from models import modelo_109
        self.dimension = models.modelo_109.dimension
        self.limit_up_V = models.modelo_109.limit_up_V
        self.limit_low_V = models.modelo_109.limit_low_V
        self.function_serial = models.modelo_109.function_serial
        self.function_parallel = models.modelo_109.function_parallel
        self.solution_global = models.modelo_109.solution_global
        self.solution_position_V = models.modelo_109.solution_position_V
        self.time_max = models.modelo_109.time_max
        self.error = models.modelo_109.error

    def load_parameters_model_110(self):
        from models import modelo_110
        self.dimension = models.modelo_110.dimension
        self.limit_up_V = models.modelo_110.limit_up_V
        self.limit_low_V = models.modelo_110.limit_low_V
        self.function_serial = models.modelo_110.function_serial
        self.function_parallel = models.modelo_110.function_parallel
        self.solution_global = models.modelo_110.solution_global
        self.solution_position_V = models.modelo_110.solution_position_V
        self.time_max = models.modelo_110.time_max
        self.error = models.modelo_110.error

    def load_parameters_model_111(self):
        from models import modelo_111
        self.dimension = models.modelo_111.dimension
        self.limit_up_V = models.modelo_111.limit_up_V
        self.limit_low_V = models.modelo_111.limit_low_V
        self.function_serial = models.modelo_111.function_serial
        self.function_parallel = models.modelo_111.function_parallel
        self.solution_global = models.modelo_111.solution_global
        self.solution_position_V = models.modelo_111.solution_position_V
        self.time_max = models.modelo_111.time_max
        self.error = models.modelo_111.error

    def load_parameters_model_112(self):
        from models import modelo_112
        self.dimension = models.modelo_112.dimension
        self.limit_up_V = models.modelo_112.limit_up_V
        self.limit_low_V = models.modelo_112.limit_low_V
        self.function_serial = models.modelo_112.function_serial
        self.function_parallel = models.modelo_112.function_parallel
        self.solution_global = models.modelo_112.solution_global
        self.solution_position_V = models.modelo_112.solution_position_V
        self.time_max = models.modelo_112.time_max
        self.error = models.modelo_112.error

