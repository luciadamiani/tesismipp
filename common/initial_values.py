import numpy as np

# Calculate initial values
class InitialValues:
    def __init__(self, model_parameters, user_parameters):
        self.x_initial = np.random.uniform(model_parameters.limit_low_V, model_parameters.limit_up_V,
                                           (user_parameters.particles_number, model_parameters.dimension)).astype(np.float32)

        self.v_initial = np.zeros((user_parameters.particles_number, model_parameters.dimension))

        self.r1 = np.random.rand(user_parameters.iterations_max, user_parameters.particles_number,
                                 model_parameters.dimension)
        self.r2 = np.random.rand(user_parameters.iterations_max, user_parameters.particles_number,
                                 model_parameters.dimension)
        kk = 0.9
        self.v_max_initial = kk * (np.array(model_parameters.limit_up_V) - np.array(model_parameters.limit_low_V)) / 2

        self.f_initial = model_parameters.function_serial(self.x_initial, user_parameters.particles_number, model_parameters.limit_up_V, model_parameters.limit_low_V)[0]
        self.TAV_initial = model_parameters.function_serial(self.x_initial, user_parameters.particles_number, model_parameters.limit_up_V, model_parameters.limit_low_V)[1]

        # Initially u is the average of all the TAV
        self.u_initial = np.sum(self.TAV_initial) / user_parameters.particles_number

        # Initially TAVP(best local of each particle) is equal to TAV, since there is no one to compare
        self.TAVP_initial = self.TAV_initial
        self.p_initial = self.x_initial
        self.TAVG_initial = np.amin(self.TAV_initial) + 0.01
        position_minTAV = np.where(self.TAV_initial <= self.TAVG_initial)
        number_minTAV = 0
        for i in range(user_parameters.particles_number):
            if self.TAV_initial[i] <= self.TAVG_initial:
                number_minTAV = number_minTAV + 1
        if number_minTAV == 1:
            position_minTAV = position_minTAV[0].item(0)
            self.fg_initial = self.f_initial[position_minTAV]
            self.g_initial = self.x_initial[position_minTAV]

        else:
            min_f = np.amin(self.f_initial[position_minTAV])
            position_minTAV_minf = np.where(self.f_initial[position_minTAV] <= min_f)
            position_minTAV_minf = position_minTAV_minf[0].item(0)
            self.fg_initial = self.f_initial[position_minTAV_minf]
            self.g_initial = self.x_initial[position_minTAV_minf]
