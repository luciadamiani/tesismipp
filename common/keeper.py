import os

import numpy as np

import xlwt

# Class that archives the results of each evaluation and translates them to an excel
class Keeper:
    def __init__(self):
        self.fg_all = []
        self.TAVG_all = []
        self.u_all = []
        self.g_all = []
        self.total_time_all = []
        self.error_all = []

    def save_run(self, results, total_time):
        self.fg_all.append(results.fg)
        self.TAVG_all.append(results.TAVG)
        self.g_all.append(results.g)
        self.u_all.append(results.u)
        self.error_all.append(results.error)
        self.total_time_all.append(total_time)

    def write_to_excel(self, user_parameters, model_parameters, archive_name):
        if os.path.exists("out_file") == 0:
            print 'Create the folder out_file'
            os.mkdir("out_file")

        fg = np.sum(self.fg_all) / user_parameters.evaluations_number
        error = np.sum(self.error_all) / user_parameters.evaluations_number
        TAVG = np.sum(self.TAVG_all) / user_parameters.evaluations_number
        u = np.sum(self.u_all) / user_parameters.evaluations_number
        total_time = np.sum(self.total_time_all) / user_parameters.evaluations_number
        promedio = np.array([fg, error, TAVG, u, total_time])
        wb = xlwt.Workbook()
        ws = wb.add_sheet('RESULTADOS')
        ws.write(0, 0, archive_name)
        ws.write(1, 1, 'fg')
        ws.write(1, 2, 'Error Relativo fg')
        ws.write(1, 3, 'TAVG')
        ws.write(1, 4, 'u')
        ws.write(1, 5, 'Tiempo Total(min)')
        ws.write(user_parameters.evaluations_number + 3, 0, 'PROMEDIO')
        x = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'x11', 'x12', 'x13', 'x14', 'x15', 'x16',
             'x17', 'x18', 'x19', 'x20', 'x21', 'x22', 'x23', 'x24', 'x25', 'x26', 'x27', 'x28', 'x29', 'x30', 'x31',
             'x32', 'x33', 'x34', 'x35', 'x36', 'x37', 'x38', 'x39', 'x40', 'x41', 'x42', 'x43', 'x44', 'x45', 'x46',
             'x47', 'x48', 'x49', 'x50', 'x51', 'x52', 'x53', 'x54', 'x55', 'x56', 'x57', 'x58', 'x59', 'x60', 'x61',
             'x62', 'x63', 'x64', 'x65', 'x66', 'x67', 'x68', 'x69', 'x70', 'x71', 'x72', 'x73', 'x74', 'x75', 'x76',
             'x77', 'x78', 'x79', 'x80', 'x81', 'x82', 'x83', 'x84', 'x85', 'x86', 'x87', 'x88', 'x89', 'x90', 'x91',
             'x92', 'x93', 'x94', 'x95', 'x96', 'x97', 'x98', 'x99', 'x100', 'x101', 'x102', 'x103', 'x104', 'x105',
             'x106', 'x107', 'x108', 'x109', 'x110', 'x111', 'x112', 'x113', 'x114', 'x115', 'x116', 'x117', 'x118',
             'x119', 'x120', 'x121', 'x122', 'x123', 'x124', 'x125', 'x126', 'x127', 'x128', 'x129', 'x130', 'x131',
             'x132', 'x133', 'x134', 'x135', 'x136', 'x137', 'x138', 'x139', 'x140', 'x141', 'x142', 'x143', 'x144',
             'x145', 'x146', 'x147', 'x148', 'x149', 'x150', 'x151', 'x152', 'x153', 'x154', 'x155', 'x156', 'x157',
             'x158', 'x159', 'x160', 'x161', 'x162', 'x163', 'x164', 'x165', 'x166', 'x167', 'x168', 'x169', 'x170',
             'x171', 'x172', 'x173', 'x174', 'x175', 'x176', 'x177', 'x178', 'x179', 'x180', 'x181', 'x182', 'x183',
             'x184', 'x185', 'x186', 'x187', 'x188', 'x189', 'x190', 'x191', 'x192', 'x193', 'x194', 'x195', 'x196',
             'x197', 'x198', 'x199', 'x200', 'x201', 'x202', 'x203', 'x204', 'x205', 'x206', 'x207', 'x208', 'x209',
             'x210', 'x211', 'x212', 'x213', 'x214', 'x215', 'x216', 'x217', 'x218', 'x219', 'x220', 'x221', 'x222',
             'x223', 'x224', 'x225', 'x226', 'x227', 'x228', 'x229', 'x230', 'x231', 'x232', 'x233', 'x234', 'x235',
             'x236', 'x237', 'x238', 'x239', 'x240', 'x241', 'x242', 'x243', 'x244', 'x245', 'x246', 'x247', 'x248',
             'x249', 'x250']

        ws.write(2, 1, model_parameters.solution_global)
        ws.write(2, 2,
                 '          ---      SOLUCION REAL      ---      SOLUCION REAL      ---      SOLUCION REAL      ---          ')
        for j in range(model_parameters.dimension):
            ws.write(1, j + 6, str(x[j]))
            ws.write(2, j + 6, str(model_parameters.solution_position_V[j]))
        for i in range(user_parameters.evaluations_number):
            ws.write(i + 3, 1, str(self.fg_all[i]))
            ws.write(i + 3, 2, str(self.error_all[i]))
            ws.write(i + 3, 3, str(self.TAVG_all[i]))
            ws.write(i + 3, 4, str(self.u_all[i]))
            ws.write(i + 3, 5, str(self.total_time_all[i]))
            for j in range(model_parameters.dimension):
                ws.write(i + 3, j + 6, str(self.g_all[i][j]))

        for j in range(5):
            ws.write(user_parameters.evaluations_number + 3, j + 1, str(promedio[j]))

        ws.write(user_parameters.evaluations_number + 5, 0, 'particles_number =')
        ws.write(user_parameters.evaluations_number + 5, 1, str(user_parameters.particles_number))
        ws.write(user_parameters.evaluations_number + 6, 0, 'dimension =')
        ws.write(user_parameters.evaluations_number + 6, 1, str(model_parameters.dimension))
        ws.write(user_parameters.evaluations_number + 7, 0, 'iterations_max =')
        ws.write(user_parameters.evaluations_number + 7, 1, str(user_parameters.iterations_max))

        ws.write(user_parameters.evaluations_number + 5, 2, 'w =')
        ws.write(user_parameters.evaluations_number + 5, 3, str(user_parameters.w))
        ws.write(user_parameters.evaluations_number + 6, 2, 'c1 =')
        ws.write(user_parameters.evaluations_number + 6, 3, str(user_parameters.c1))
        ws.write(user_parameters.evaluations_number + 7, 2, 'c2 =')
        ws.write(user_parameters.evaluations_number + 7, 3, str(user_parameters.c2))

        wb.save("out_file/" + archive_name)
