import time
from common.model_parameters import ModelParameters
from common.initial_values import InitialValues
from common.input import *
from common.keeper import Keeper
from common.model_parameters import ModelParameters
from solvers.PSOparallel_reduccionparalelo_localmemory import PSOParallel
from solvers.PSOserial import PSOSerial
import numpy as np


number_reductions = 50
reduction_V = np.array([0.3]*number_reductions)


def run_pso():
    # Read user parameters
    initial_time = time.time()
    user_parameters = read_input()
    print(user_parameters)

    # Read model parameters
    model_parameters = ModelParameters(user_parameters.model_id)

    limit_up_original_V = model_parameters.limit_up_V
    limit_low_original_V =model_parameters.limit_low_V

    keeper_serial = Keeper()
    solver_serial = PSOSerial(user_parameters, model_parameters)

    if user_parameters.process_type == PROCESS_TYPE_PARALLEL or user_parameters.process_type == PROCESS_TYPE_PARALLEL_AND_SERIAL:
        keeper_parallel = Keeper()
        solver_parallel = PSOParallel(user_parameters, model_parameters)

    # Calculate the initial values
    initial_values = InitialValues(model_parameters, user_parameters)
    g_initial_original=initial_values.g_initial
    fg_initial_original=initial_values.fg_initial
    TAVG_initial_original=initial_values.TAVG_initial


    # Run PSO serial
    if user_parameters.process_type == PROCESS_TYPE_SERIAL:
        # Loop evaluation, created to evaluate "user_parameters.evaluations_number" times the same model function
        for evaluation in range(user_parameters.evaluations_number):

            for iteration_reduction in range(number_reductions):

                if iteration_reduction != 0:
                    model_parameters.limit_up_V = initial_values.g_initial * (1 + reduction_V[iteration_reduction])
                    model_parameters.limit_low_V = initial_values.g_initial * (1 - reduction_V[iteration_reduction])
                    model_parameters.limit_up_V = np.clip(model_parameters.limit_up_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_up_V)
                    model_parameters.limit_low_V = np.clip(model_parameters.limit_low_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_low_V)

                serial_results = solver_serial.compute(initial_values)

                if serial_results.TAVG <= 0.00001 and initial_values.TAVG_initial <= 0.00001:
                    if serial_results.fg <= initial_values.fg_initial:
                        initial_values.fg_initial = serial_results.fg
                        initial_values.g_initial = serial_results.g
                        initial_values.TAVG_initial = serial_results.TAVG

                if serial_results.TAVG <= 0.00001 and initial_values.TAVG_initial > 0.00001:
                    initial_values.fg_initial = serial_results.fg
                    initial_values.g_initial = serial_results.g
                    initial_values.TAVG_initial = serial_results.TAVG

                if serial_results.TAVG > 0.00001 and initial_values.TAVG_initial > 0.00001:
                    if serial_results.TAVG <= initial_values.TAVG_initial:
                        initial_values.fg_initial = serial_results.fg
                        initial_values.g_initial = serial_results.g
                        initial_values.TAVG_initial = serial_results.TAVG

                serial_results.fg = initial_values.fg_initial
                serial_results.g = initial_values.g_initial
                serial_results.TAVG = initial_values.TAVG_initial

            final_time_serial = time.time()
            total_time_serial = final_time_serial - initial_time

            keeper_serial.save_run(serial_results, total_time_serial)

            if evaluation == user_parameters.evaluations_number - 1:
                keeper_serial.write_to_excel(user_parameters, model_parameters, user_parameters.out_file_name_serial)

    # Run PSO parallel
    if user_parameters.process_type == PROCESS_TYPE_PARALLEL:
        # Loop evaluation, created to evaluate "user_parameters.evaluations_number" times the same model function
        for evaluation in range(user_parameters.evaluations_number):
            for iteration_reduction in range(number_reductions):

                if iteration_reduction != 0:
                    model_parameters.limit_up_V = initial_values.g_initial * (1 + reduction_V[iteration_reduction])
                    model_parameters.limit_low_V = initial_values.g_initial * (1 - reduction_V[iteration_reduction])
                    model_parameters.limit_up_V = np.clip(model_parameters.limit_up_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_up_V)
                    model_parameters.limit_low_V = np.clip(model_parameters.limit_low_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_low_V)

                parallel_results = solver_parallel.compute(initial_values)

                if serial_results.TAVG <= 0.00001 and initial_values.TAVG_initial <= 0.00001:
                    if serial_results.fg <= initial_values.fg_initial:
                        initial_values.fg_initial = serial_results.fg
                        initial_values.g_initial = serial_results.g
                        initial_values.TAVG_initial = serial_results.TAVG
                if serial_results.TAVG <= 0.00001 and initial_values.TAVG_initial > 0.00001:
                    initial_values.fg_initial = serial_results.fg
                    initial_values.g_initial = serial_results.g
                    initial_values.TAVG_initial = serial_results.TAVG
                if serial_results.TAVG > 0.00001 and initial_values.TAVG_initial > 0.00001:
                    if serial_results.TAVG <= initial_values.TAVG_initial:
                        initial_values.fg_initial = serial_results.fg
                        initial_values.g_initial = serial_results.g
                        initial_values.TAVG_initial = serial_results.TAVG

                serial_results.fg = initial_values.fg_initial
                serial_results.g = initial_values.g_initial
                serial_results.TAVG = initial_values.TAVG_initial

            final_time_parallel = time.time()
            total_time_parallel = final_time_parallel - initial_time

            keeper_parallel.save_run(parallel_results, total_time_parallel)

            if evaluation == user_parameters.evaluations_number - 1:
                keeper_parallel.write_to_excel(user_parameters, model_parameters, user_parameters.out_file_name_parallel)

    # Run PSO serial and parallel
    media_time=time.time()-initial_time

    if user_parameters.process_type == PROCESS_TYPE_PARALLEL_AND_SERIAL:
        # Loop evaluation, created to evaluate "user_parameters.evaluations_number" times the same model function
        for evaluation in range(user_parameters.evaluations_number):
            initial_time_parallel = time.time()
            for iteration_reduction in range(number_reductions):

                if iteration_reduction != 0:
                    model_parameters.limit_up_V = initial_values.g_initial * (1 + reduction_V[iteration_reduction])
                    model_parameters.limit_low_V = initial_values.g_initial * (1 - reduction_V[iteration_reduction])
                    model_parameters.limit_up_V = np.clip(model_parameters.limit_up_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_up_V)
                    model_parameters.limit_low_V = np.clip(model_parameters.limit_low_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_low_V)

                parallel_results = solver_parallel.compute(initial_values)

                if parallel_results.TAVG <= 0.00001 and initial_values.TAVG_initial <= 0.00001:
                    if parallel_results.fg <= initial_values.fg_initial:
                        initial_values.fg_initial = parallel_results.fg
                        initial_values.g_initial = parallel_results.g
                        initial_values.TAVG_initial = parallel_results.TAVG
                if parallel_results.TAVG <= 0.00001 and initial_values.TAVG_initial > 0.00001:
                    initial_values.fg_initial = parallel_results.fg
                    initial_values.g_initial = parallel_results.g
                    initial_values.TAVG_initial = parallel_results.TAVG
                if parallel_results.TAVG > 0.00001 and initial_values.TAVG_initial > 0.00001:
                    if parallel_results.TAVG <= initial_values.TAVG_initial:
                        initial_values.fg_initial = parallel_results.fg
                        initial_values.g_initial = parallel_results.g
                        initial_values.TAVG_initial = parallel_results.TAVG

                parallel_results.fg = initial_values.fg_initial
                parallel_results.g = initial_values.g_initial
                parallel_results.TAVG = initial_values.TAVG_initial
            # print "PARALELO iteration_reduction", iteration_reduction
            # print "parallel_results.fg", parallel_results.fg

            final_time_parallel = time.time()
            total_time_parallel = final_time_parallel - initial_time_parallel+media_time
            keeper_parallel.save_run(parallel_results, total_time_parallel)

            initial_time_serial=time.time()
            for iteration_reduction in range(number_reductions):

                if iteration_reduction == 0:
                    initial_values.g_initial = g_initial_original
                    initial_values.fg_initial = fg_initial_original
                    initial_values.TAVG_initial = TAVG_initial_original

                if iteration_reduction != 0:
                    model_parameters.limit_up_V = initial_values.g_initial * (1 + reduction_V[iteration_reduction])
                    model_parameters.limit_low_V = initial_values.g_initial * (1 - reduction_V[iteration_reduction])
                    model_parameters.limit_up_V = np.clip(model_parameters.limit_up_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_up_V)
                    model_parameters.limit_low_V = np.clip(model_parameters.limit_low_V, limit_low_original_V, limit_up_original_V, out=model_parameters.limit_low_V)

                serial_results = solver_serial.compute(initial_values)

                if serial_results.TAVG <= 0.00001 and initial_values.TAVG_initial <= 0.00001:
                    if serial_results.fg <= initial_values.fg_initial:
                        initial_values.fg_initial = serial_results.fg
                        initial_values.g_initial = serial_results.g
                        initial_values.TAVG_initial = serial_results.TAVG
                if serial_results.TAVG <= 0.00001 and initial_values.TAVG_initial > 0.00001:
                    initial_values.fg_initial = serial_results.fg
                    initial_values.g_initial = serial_results.g
                    initial_values.TAVG_initial = serial_results.TAVG
                if serial_results.TAVG > 0.00001 and initial_values.TAVG_initial > 0.00001:
                    if serial_results.TAVG <= initial_values.TAVG_initial:
                        initial_values.fg_initial = serial_results.fg
                        initial_values.g_initial = serial_results.g
                        initial_values.TAVG_initial = serial_results.TAVG

                serial_results.fg = initial_values.fg_initial
                serial_results.g = initial_values.g_initial
                serial_results.TAVG = initial_values.TAVG_initial

                # print "SERIE iteration_reduction",iteration_reduction
                # print "serial_results.fg",serial_results.fg

            final_time_serial = time.time()
            total_time_serial = final_time_serial - initial_time_serial + media_time
            keeper_serial.save_run(serial_results, total_time_serial)

            # Save serial and parallel results in excel
            if evaluation == user_parameters.evaluations_number - 1:
                keeper_parallel.write_to_excel(user_parameters, model_parameters, user_parameters.out_file_name_parallel)
                keeper_serial.write_to_excel(user_parameters, model_parameters, user_parameters.out_file_name_serial)


if __name__ == '__main__':
    run_pso()
